<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk" sourcelanguage="en">
  <context>
    <name>MainMenu</name>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="301"/>
      <source>Settings</source>
      <translation>Налаштування</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="225"/>
      <source>Slideshow</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Слайд-шоу</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="86"/>
      <source>Rename file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Перейменувати файл</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="91"/>
      <source>Copy file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Копіювати файл</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="96"/>
      <source>Move file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Перемістити файл</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="101"/>
      <source>Delete file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Видалити файл</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="114"/>
      <source>Tag faces</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Позначити обличчя</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="132"/>
      <source>Hide metadata</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Сховати метадані</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="132"/>
      <source>Show metadata</source>
      <translation>Показати метадані</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="137"/>
      <source>Hide histogram</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Сховати гістограму</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="137"/>
      <source>Show histogram</source>
      <translation>Показати гістограму</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="124"/>
      <source>Set as wallpaper</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Встановити як шпалери</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="315"/>
      <source>Quit</source>
      <translation>Вихід</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="119"/>
      <source>Scale image</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Масштабувати зображення</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="165"/>
      <source>Navigation</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Навігація</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="169"/>
      <source>Browse images</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Перегляд зображень</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="173"/>
      <source>first</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>першого</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="176"/>
      <source>last</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>останнього</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="185"/>
      <source>Zoom</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Зум</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="189"/>
      <source>Actual size</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Справжній розмір</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="201"/>
      <source>Rotation/Flip</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Обертання/перевертання</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="205"/>
      <source>Horizontal flip</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Горизонтальне перевертання</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="209"/>
      <source>Vertical flip</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Вертикальне перевертання</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="213"/>
      <source>Reset flip</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Скинути обертання</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="217"/>
      <source>Reset rotation</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Скинути перевертання</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="229"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="242"/>
      <source>Start</source>
      <extracomment>This is an entry in the main menu on the right, used as in &quot;START slideshow/sorting&quot;. Please keep short!</extracomment>
      <translation>Почати</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="233"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="246"/>
      <source>Setup</source>
      <extracomment>This is an entry in the main menu on the right, used as in &quot;SETUP slideshow/sorting&quot;. Please keep short!</extracomment>
      <translation>Налаштування</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="238"/>
      <source>Advanced Sort</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Розширене сортування</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="251"/>
      <source>Other</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Інше</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="254"/>
      <source>Filter images</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Фільтрувати зображення</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="269"/>
      <source>External</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Зовнішній</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="304"/>
      <source>About</source>
      <translation>Про застосунок</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="312"/>
      <source>Online help</source>
      <translation>Онлайн-довідка</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="117"/>
      <source>Click and drag to resize main menu</source>
      <translation>Натисніть і перетягніть, щоб змінити розмір головного меню</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="193"/>
      <source>Reset zoom</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Скинути масштаб</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="258"/>
      <source>Streaming (Chromecast)</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Потокова передача (Chromecast)</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="109"/>
      <source>Copy to clipboard</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Копіювати в буфер обміну</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="262"/>
      <source>Open in default file manager</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Відкрити у типовому файловому менеджері</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenuPopout.qml" line="34"/>
      <source>Main Menu</source>
      <extracomment>Window title</extracomment>
      <translation>Головне меню</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenuGroup.qml" line="100"/>
      <location filename="../qml/menumeta/PQMainMenuGroup.qml" line="201"/>
      <source>You need to load an image first.</source>
      <translation>Спочатку потрібно завантажити зображення.</translation>
    </message>
  </context>
  <context>
    <name>PQImageFormats</name>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="48"/>
      <location filename="../cplusplus/settings/imageformats.cpp" line="60"/>
      <source>ERROR getting default image formats</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>ПОМИЛКА отримання стандартних форматів зображення</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="49"/>
      <source>I tried hard, but I just cannot open even a read-only version of the database of default image formats.</source>
      <translation>Я дуже старався, але я просто не можу відкрити навіть версію призначену лише для читання бази даних стандартних форматів зображень.</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="49"/>
      <location filename="../cplusplus/settings/imageformats.cpp" line="61"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>Десь щось пішло не так!</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="61"/>
      <source>I tried hard, but I just cannot open the database of default image formats.</source>
      <translation>Я дуже старався, але просто не можу відкрити базу даних стандартних форматів зображень.</translation>
    </message>
  </context>
  <context>
    <name>PQMetaData</name>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="434"/>
      <source>yes</source>
      <extracomment>This string identifies that flash was fired, stored in image metadata</extracomment>
      <translation>так</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="436"/>
      <source>no</source>
      <extracomment>This string identifies that flash was not fired, stored in image metadata</extracomment>
      <translation>ні</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="438"/>
      <source>No flash function</source>
      <extracomment>This string refers to the absense of a flash, stored in image metadata</extracomment>
      <translation>Без функції спалаху</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="440"/>
      <source>strobe return light not detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>зворотне світло стробоскопа не виявлено</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="442"/>
      <source>strobe return light detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>виявлено зворотне світло стробоскопа</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="444"/>
      <source>compulsory flash mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>примусовий режим спалаху</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="446"/>
      <source>auto mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Автоматичний режим</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="448"/>
      <source>red-eye reduction mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Коригування ефекту «Червоних очей»</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="450"/>
      <source>return light detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>виявлено зворотне світло</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="452"/>
      <source>return light not detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>зворотне світло не виявлено</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="500"/>
      <source>Invalid flash</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Недійсний спалах</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="511"/>
      <source>Standard</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Стандартний</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="514"/>
      <source>Landscape</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Альбомна орієнтація</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="517"/>
      <source>Portrait</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Книжкова орієнтація</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="520"/>
      <source>Night Scene</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Нічная сцена</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="523"/>
      <source>Invalid Scene Type</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Недійсний тип сцени</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="576"/>
      <source>Unknown</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Невідомий</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="579"/>
      <source>Daylight</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Денне світло</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="582"/>
      <source>Fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Флуоресцентний</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="585"/>
      <source>Tungsten (incandescent light)</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Вольфрам (лампа розжарювання)</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="588"/>
      <source>Flash</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Спалах</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="591"/>
      <source>Fine weather</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Гарна погода</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="594"/>
      <source>Cloudy Weather</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Хмарна погода</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="597"/>
      <source>Shade</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Тінь</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="600"/>
      <source>Daylight fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Денне світло флуоресцентне</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="603"/>
      <source>Day white fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Денний білий флуоресцентний</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="606"/>
      <source>Cool white fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Холодний білий флуоресцентний</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="609"/>
      <source>White fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Білий флуоресцентний</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="612"/>
      <location filename="../cplusplus/scripts/metadata.cpp" line="615"/>
      <location filename="../cplusplus/scripts/metadata.cpp" line="618"/>
      <source>Standard light</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Стандартне світло</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="631"/>
      <source>Other light source</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Інше джерело світла</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="634"/>
      <source>Invalid light source</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Неправильне джерело світла</translation>
    </message>
  </context>
  <context>
    <name>PQPrintSupport</name>
    <message>
      <location filename="../cplusplus/print/printsupport.cpp" line="52"/>
      <source>Print</source>
      <translation>Видрукувати</translation>
    </message>
  </context>
  <context>
    <name>PQSettings</name>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="63"/>
      <source>ERROR getting database with default settings</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>ПОМИЛКА отримання бази даних із налаштуваннями за замовчуванням</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="64"/>
      <source>I tried hard, but I just cannot open even a read-only version of the settings database.</source>
      <translation>Я дуже старався, але я просто не можу навіть відкрити версію бази даних налаштувань, доступну лише для читання.</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="64"/>
      <location filename="../cplusplus/settings/settings.cpp" line="76"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>Десь щось пішло не так!</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="75"/>
      <source>ERROR opening database with default settings</source>
      <translation>ПОМИЛКА відкриття бази даних із налаштуваннями за замовчуванням</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="76"/>
      <source>I tried hard, but I just cannot open the database of default settings.</source>
      <translation>Я дуже старався, але просто не можу відкрити базу даних налаштувань за замовчуванням.</translation>
    </message>
  </context>
  <context>
    <name>PQShortcuts</name>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="48"/>
      <source>ERROR getting database with default shortcuts</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>ПОМИЛКА отримання бази даних зі стандартними ярликами</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="49"/>
      <source>I tried hard, but I just cannot open even a read-only version of the shortcuts database.</source>
      <translation>Я дуже старався, але я просто не можу навіть відкрити версію бази даних ярликів лише для читання.</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="49"/>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="61"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>Десь щось пішло не так!</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="60"/>
      <source>ERROR opening database with default settings</source>
      <translation>ПОМИЛКА відкриття бази даних із налаштуваннями за замовчуванням</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="61"/>
      <source>I tried hard, but I just cannot open the database of default shortcuts.</source>
      <translation>Я дуже старався, але просто не можу відкрити базу даних стандартних ярликів.</translation>
    </message>
  </context>
  <context>
    <name>PQStartup</name>
    <message>
      <location filename="../cplusplus/startup/startup.cpp" line="56"/>
      <source>SQLite error</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>Помилка SQLite</translation>
    </message>
    <message>
      <location filename="../cplusplus/startup/startup.cpp" line="57"/>
      <source>You seem to be missing the SQLite driver for Qt. This is needed though for a few different things, like reading and writing the settings. Without it, PhotoQt cannot function!</source>
      <translation>Здається, вам не вистачає драйвера SQLite для Qt. Хоча це потрібно для кількох інших речей, наприклад для читання та запису налаштувань. Без цього PhotoQt не може працювати!</translation>
    </message>
  </context>
  <context>
    <name>TabShortcuts</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/TabShortcuts.qml" line="245"/>
      <source/>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>about</name>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="52"/>
      <location filename="../qml/about/PQAbout.qml" line="165"/>
      <source>Close</source>
      <translation>Закрити</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="98"/>
      <source>Current version:</source>
      <translation>Поточна версія:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="100"/>
      <source>Show configuration overview</source>
      <extracomment>The &apos;configuration&apos; talked about here refers to the configuration at compile time, i.e., which image libraries were enabled and which versions</extracomment>
      <translation>Показати вигляд конфігурації</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="110"/>
      <source>License:</source>
      <translation>Ліцензія:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="115"/>
      <source>Open license in browser</source>
      <translation>Відкрити ліцензію у браузері</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="135"/>
      <source>Open website in browser</source>
      <translation>Відкрити сайт у браузері</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="203"/>
      <source>Configuration</source>
      <extracomment>The &apos;configuration&apos; talked about here refers to the configuration at compile time, i.e., which image libraries were enabled and which versions</extracomment>
      <translation>Конфігурація</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="222"/>
      <source>Copy to clipboard</source>
      <translation>Копіювати в буфер обміну</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="130"/>
      <source>Website:</source>
      <translation>Вебсайт:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="145"/>
      <source>Contact:</source>
      <translation>Контакти:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="150"/>
      <source>Send an email</source>
      <translation>Відправити e-mail</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAboutPopout.qml" line="34"/>
      <source>About</source>
      <extracomment>Window title</extracomment>
      <translation>Про застосунок</translation>
    </message>
  </context>
  <context>
    <name>advancedsort</name>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="83"/>
      <location filename="../qml/advancedsort/PQAdvancedSortPopout.qml" line="34"/>
      <source>Advanced Image Sort</source>
      <extracomment>Window title</extracomment>
      <translation>Розширене сортування зображень</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="94"/>
      <source>It is possible to sort the images in the current folder by color properties. Depending on the number of images and the settings, this might take a few seconds.</source>
      <translation>Зображення в поточній папці можна сортувати за властивостями кольору. Залежно від кількості зображень і налаштувань це може зайняти кілька секунд.</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="107"/>
      <source>Sort by:</source>
      <extracomment>Used as &apos;sort by dominant/average color&apos;</extracomment>
      <translation>Сортувати за:</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="116"/>
      <source>Resolution</source>
      <extracomment>The image resolution (width/height in pixels)</extracomment>
      <translation>Роздільна здатність</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="118"/>
      <source>Dominant color</source>
      <extracomment>The color that is most common in the image</extracomment>
      <translation>Домінуючий колір</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="120"/>
      <source>Average color</source>
      <extracomment>the average color of the image</extracomment>
      <translation>Середній колір</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="122"/>
      <source>Luminosity</source>
      <extracomment>the average color of the image</extracomment>
      <translation>Освітленість</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="137"/>
      <source>ascending</source>
      <extracomment>sort order, i.e., &apos;ascending order&apos;</extracomment>
      <translation>за зростанням</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="142"/>
      <source>descending</source>
      <extracomment>sort order, i.e., &apos;descending order&apos;</extracomment>
      <translation>за спаданням</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="159"/>
      <source>speed vs quality:</source>
      <extracomment>Please keep short! Sorting images by color comes with a speed vs quality tradeoff.</extracomment>
      <translation>швидкість або якість:</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="165"/>
      <source>low quality (fast)</source>
      <extracomment>quality and speed of sorting image by color</extracomment>
      <translation>низька якість (швидко)</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="167"/>
      <source>medium quality</source>
      <extracomment>quality and speed of sorting image by color</extracomment>
      <translation>середня якість</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="169"/>
      <source>high quality (slow)</source>
      <extracomment>quality and speed of sorting image by color</extracomment>
      <translation>висока якість (повільно)</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="181"/>
      <source>There is also a quickstart shortcut that immediately starts the sorting using the latest settings.</source>
      <translation>Існує також ярлик швидкого запуску, який негайно запускає сортування з використанням останніх налаштувань.</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="201"/>
      <source>Sort images</source>
      <extracomment>Written on a clickable button - please keep short</extracomment>
      <translation>Сортувати зображення</translation>
    </message>
  </context>
  <context>
    <name>buttongeneric</name>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="79"/>
      <source>Ok</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Так</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="81"/>
      <source>Cancel</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Скасувати</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="83"/>
      <source>Save</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Зберегти</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="85"/>
      <source>Close</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Закрити</translation>
    </message>
  </context>
  <context>
    <name>commandlineparser</name>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="51"/>
      <source>Image Viewer</source>
      <translation>Переглядач зображень</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="53"/>
      <source>Image file to open.</source>
      <translation>Відкрити зображення.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="60"/>
      <source>Make PhotoQt ask for a new file.</source>
      <translation>Робити запит PhotoQt на новий файл.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="62"/>
      <source>Shows PhotoQt from system tray.</source>
      <translation>Показує PhotoQt в системному треї.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="64"/>
      <source>Hides PhotoQt to system tray.</source>
      <extracomment>Command line option</extracomment>
      <translation>Приховує PhotoQt до системного трею.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="66"/>
      <source>Show/Hide PhotoQt.</source>
      <translation>Показати/приховати PhotoQt.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="68"/>
      <source>Enable thumbnails.</source>
      <extracomment>Command line option</extracomment>
      <translation>Увімкнути мініатюри.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="70"/>
      <source>Disable thumbnails.</source>
      <extracomment>Command line option</extracomment>
      <translation>Вимкнути мініатюри.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="72"/>
      <source>Enable system tray icon.</source>
      <extracomment>Command line option</extracomment>
      <translation>Показувати іконку в системному треї.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="74"/>
      <source>Disable system tray icon.</source>
      <extracomment>Command line option</extracomment>
      <translation>Вимкнути іконку в системному треї.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="76"/>
      <source>Start PhotoQt hidden to the system tray.</source>
      <extracomment>Command line option</extracomment>
      <translation>Запустити PhotoQt прихованим в системному треї.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="78"/>
      <source>Open standalone PhotoQt, allows for multiple instances but without remote interaction.</source>
      <extracomment>Command line option</extracomment>
      <translation>Відкритий автономний PhotoQt, дозволяє створювати кілька екземплярів, але без віддаленої взаємодії.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="80"/>
      <source>Simulate a shortcut sequence</source>
      <extracomment>Command line option</extracomment>
      <translation>Імітація послідовності клавіш</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="82"/>
      <source>Switch on debug messages.</source>
      <extracomment>Command line option</extracomment>
      <translation>Увімкніть повідомлення про налагодження.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="84"/>
      <source>Switch off debug messages.</source>
      <extracomment>Command line option</extracomment>
      <translation>Вимкніть повідомлення про налагодження.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="86"/>
      <source>Export configuration to given filename.</source>
      <extracomment>Command line option</extracomment>
      <translation>Експортувати конфігурацію до заданого імені файлу.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="88"/>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="92"/>
      <source>filename</source>
      <extracomment>Command line option</extracomment>
      <translation>ім'я файлу</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="90"/>
      <source>Import configuration from given filename.</source>
      <extracomment>Command line option</extracomment>
      <translation>Імпорт конфігурації з указаного імені файлу.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="94"/>
      <source>Check the configuration and correct any detected issues.</source>
      <extracomment>Command line option</extracomment>
      <translation>Перевірте конфігурацію та виправте всі виявлені проблеми.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="96"/>
      <source>Reset default configuration.</source>
      <extracomment>Command line option</extracomment>
      <translation>Скинути налаштування за замовчуванням.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="98"/>
      <source>Show configuration overview.</source>
      <extracomment>Command line option</extracomment>
      <translation>Показати огляд конфігурації.</translation>
    </message>
  </context>
  <context>
    <name>facetagging</name>
    <message>
      <location filename="../qml/mainwindow/image/PQFaceTagger.qml" line="194"/>
      <source>Who is this?</source>
      <extracomment>This question is asked in the face tagger to ask for the name of a tagged face</extracomment>
      <translation>Хто це?</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/image/PQFaceTagger.qml" line="201"/>
      <source>Enter name</source>
      <translation>Введіть ім'я</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQMessage.qml" line="62"/>
      <source>Click to exit face tagging mode</source>
      <translation>Натисніть, щоб вийти з режиму позначення обличчя</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQMessage.qml" line="75"/>
      <source>Click to tag faces, changes are saved automatically</source>
      <translation>Натисніть, щоб позначити обличчя, зміни зберігаються автоматично</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/image/PQFaceTagsUnsupported.qml" line="60"/>
      <source>File type does not support face tags.</source>
      <translation>Тип файлу не підтримує позначення обличчя.</translation>
    </message>
  </context>
  <context>
    <name>filedialog</name>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="71"/>
      <source>Backwards</source>
      <translation>Назад</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="93"/>
      <source>Up a level</source>
      <translation>На рівень вище</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="119"/>
      <source>Forwards</source>
      <translation>Далі</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="186"/>
      <source>List subfolders</source>
      <translation>Список вкладених папок</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="262"/>
      <source>Enter fullscreen</source>
      <translation>Перейти в повноекранний режим</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="263"/>
      <source>Exit fullscreen</source>
      <translation>Вийти з повноекранного режиму</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="293"/>
      <source>Close</source>
      <translation>Закрити</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQDevices.qml" line="109"/>
      <source>Storage devices</source>
      <extracomment>This is the category title of storage devices to open (like USB keys) in the element for opening files</extracomment>
      <translation>Пристрої зберігання</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQDevices.qml" line="141"/>
      <source>Detected storage devices on your system</source>
      <translation>Виявлені пристрої зберігання даних у вашій системі</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="121"/>
      <source>no supported files/folders found</source>
      <translation>не знайдено підтримуваних файлів/папок</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="228"/>
      <source>Click and drag to favorites</source>
      <translation>Натисніть і перетягніть до обраного</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="355"/>
      <source># images</source>
      <translation># зображень</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="356"/>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="375"/>
      <source>Date:</source>
      <translation>Дата:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="357"/>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="376"/>
      <source>Time:</source>
      <translation>Час:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="373"/>
      <source>File size:</source>
      <translation>Розмір файлу:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="374"/>
      <source>File type:</source>
      <translation>Тип файлу:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="458"/>
      <source>%1 image</source>
      <translation>%1 зображення</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="460"/>
      <source>%1 images</source>
      <translation>%1 зображень</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="50"/>
      <source>no folder added to favorites yet</source>
      <extracomment>&apos;favorites&apos; here refers to the list of favorite folders a user can set in the file dialog</extracomment>
      <translation>жодна папка ще не додана до обраного</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="154"/>
      <source>Favorites</source>
      <extracomment>This is the category title of user-set folders (or favorites) in the file dialog</extracomment>
      <translation>Обране</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="170"/>
      <source>Your favorites</source>
      <translation>Ваші обрані</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="220"/>
      <source>Show entry</source>
      <translation>Показати запис</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="220"/>
      <source>Hide entry</source>
      <translation>Приховати запис</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="221"/>
      <source>Remove entry</source>
      <translation>Видалити запис</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="222"/>
      <source>Hide hidden entries</source>
      <translation>Приховати приховані записи</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="222"/>
      <source>Show hidden entries</source>
      <translation>Показати приховані записи</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="43"/>
      <source>Load this folder</source>
      <translation>Завантажити цю папку</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="43"/>
      <source>Load this file</source>
      <translation>Завантажити цей файл</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="56"/>
      <source>Add to Favorites</source>
      <translation>Додати в Обране</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="78"/>
      <source>Show tooltip with image details</source>
      <translation>Показати підказку з деталями зображення</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="88"/>
      <source>Visible</source>
      <extracomment>This is a context menu entry, referring to whether the large preview image is VISIBLE</extracomment>
      <translation>Видимий</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="96"/>
      <source>Higher resolution</source>
      <extracomment>This is a context menu entry, referring to whether a preview image with a HIGHER RESOLUTION should be loaded</extracomment>
      <translation>Більша роздільна здатність</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="104"/>
      <source>Blurred image</source>
      <extracomment>This is a context menu entry, selecting it will BLUR the preview IMAGE</extracomment>
      <translation>Розмите зображення</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="112"/>
      <source>Muted colors</source>
      <extracomment>This is a context menu entry, selecting it will make the COLORS of the preview image MUTED</extracomment>
      <translation>Приглушені кольори</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="64"/>
      <source>Show hidden files</source>
      <translation>Показувати приховані файли</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="71"/>
      <source>Show thumbnails</source>
      <translation>Показувати мініатюри</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQStandard.qml" line="103"/>
      <source>Standard</source>
      <extracomment>This is the category title of user-set folders (or favorites) in the file dialog</extracomment>
      <translation>Стандартний</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQStandard.qml" line="119"/>
      <source>Some standard locations</source>
      <translation>Деякі стандартні розташування</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="48"/>
      <source>Zoom:</source>
      <translation>Масштаб:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="57"/>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="72"/>
      <source>Adjust font size of files and folders</source>
      <translation>Регулювання розміру шрифту файлів і папок</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="73"/>
      <source>Zoom factor:</source>
      <translation>Коефіцієнт масштабування:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="88"/>
      <source>Sort by:</source>
      <translation>Сортувати за:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="90"/>
      <source>Name</source>
      <translation>Ім’я</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="91"/>
      <source>Natural Name</source>
      <translation>Звичайне ім'я</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="92"/>
      <source>Time modified</source>
      <translation>Час зміни</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="93"/>
      <source>File size</source>
      <translation>Розмір файлу</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="94"/>
      <source>File type</source>
      <translation>Тип файлу</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="95"/>
      <source>reverse order</source>
      <translation>У зворотному порядку</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="102"/>
      <source>Choose by what to sort the files</source>
      <translation>Виберіть як саме сортувати файли</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="138"/>
      <source>All supported images</source>
      <translation>Усі підтримувані зображення</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="143"/>
      <source>Video files</source>
      <translation>Відеофайли</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="144"/>
      <source>All files</source>
      <translation>Всі файли</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="154"/>
      <source>Choose which selection of files to show</source>
      <translation>Оберіть які саме файли показати</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="174"/>
      <source>Remember loaded folder between sessions.</source>
      <translation>Запам'ятовувати завантажену папку між сеансами.</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="196"/>
      <source>Switch between list and icon view</source>
      <translation>Перемикання між списком і показом значків</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="116"/>
      <source>Hide standard locations</source>
      <translation>Приховати стандартні розташування</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="116"/>
      <source>Show standard locations</source>
      <translation>Показувати стандартні розташування</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="117"/>
      <source>Hide favorite locations</source>
      <translation>Приховати улюблені місця розташування</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="117"/>
      <source>Show favorite locations</source>
      <translation>Показувати улюблені місця розташування</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="118"/>
      <source>Hide storage devices</source>
      <translation>Приховати накопичувачі</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="118"/>
      <source>Show storage devices</source>
      <translation>Показати накопичувачі</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialogPopout.qml" line="34"/>
      <source>File dialog</source>
      <extracomment>Window title</extracomment>
      <translation>Файловий діалог</translation>
    </message>
  </context>
  <context>
    <name>filemanagement</name>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="83"/>
      <location filename="../qml/filemanagement/PQDeletePopout.qml" line="34"/>
      <source>Delete file?</source>
      <extracomment>Window title</extracomment>
      <translation>Видалити файл?</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="101"/>
      <source>An error occured, file could not be deleted!</source>
      <translation>Сталася помилка, файл не може бути видалений!</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="121"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="177"/>
      <source>Move to trash</source>
      <translation>Перемістити у кошик</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="137"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="180"/>
      <source>Delete permanently</source>
      <translation>Видалити остаточно</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQRename.qml" line="83"/>
      <location filename="../qml/filemanagement/PQRename.qml" line="132"/>
      <location filename="../qml/filemanagement/PQRenamePopout.qml" line="34"/>
      <source>Rename file</source>
      <extracomment>Window title</extracomment>
      <translation>Перейменувати файл</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQRename.qml" line="101"/>
      <source>An error occured, file could not be renamed!</source>
      <translation>Сталася помилка, файл не може бути перейменований!</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQRename.qml" line="112"/>
      <source>Enter new filename</source>
      <translation>Введіть нову назву файлу</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="82"/>
      <location filename="../qml/filemanagement/PQSaveAsPopout.qml" line="34"/>
      <source>Save file as</source>
      <extracomment>This is a title, similar to all the &apos;save as&apos; options in many programs.
----------
Window title</extracomment>
      <translation>Зберегти файл як</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="105"/>
      <source>An error occured, file could not be saved!</source>
      <translation>Сталася помилка, файл не вдалося зберегти!</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="116"/>
      <source>Operation cancelled</source>
      <extracomment>&apos;Operation&apos; here is the operation of saving an image in a new format</extracomment>
      <translation>Операцію скасовано</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="124"/>
      <source>Filter formats</source>
      <extracomment>This is a short hint informing the user that here they can &apos;filter all the possible file formats&apos;</extracomment>
      <translation>Формати фільтру</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="223"/>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="230"/>
      <source>New filename</source>
      <translation>Нова назва файлу</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="251"/>
      <source>Choose location and save file</source>
      <translation>Виберіть місце та збережіть файл</translation>
    </message>
  </context>
  <context>
    <name>filter</name>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="90"/>
      <source>Filter images in current directory</source>
      <translation>Фільтрувати зображення в поточному каталозі</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="102"/>
      <source>To filter by file extension, start the term with a dot. Setting the width or height of the resolution to 0 ignores that dimension.</source>
      <translation>Щоб фільтрувати за розширенням файлу, починайте умову із крапки. Встановлення ширини або висоти роздільної здатності на 0 ігнорує цей розмір.</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="120"/>
      <source>File name/extension:</source>
      <translation>Ім'я/розширення файлу:</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="132"/>
      <source>Enter terms</source>
      <translation>Введіть умови</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="161"/>
      <location filename="../qml/filter/PQFilter.qml" line="213"/>
      <source>greater than</source>
      <extracomment>used as tooltip in the sense of &apos;image resolution GREATER THAN 123x123&apos;
----------
used as tooltip in the sense of &apos;file size GREATER THAN 123 KB/MB&apos;</extracomment>
      <translation>більше ніж</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="163"/>
      <location filename="../qml/filter/PQFilter.qml" line="215"/>
      <source>less than</source>
      <extracomment>used as tooltip in the sense of &apos;image resolution LESS THAN 123x123&apos;
----------
used as tooltip in the sense of &apos;file size LESS THAN 123 KB/MB&apos;</extracomment>
      <translation>менше ніж</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="200"/>
      <source>File size</source>
      <translation>Розмір файлу</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="260"/>
      <source>Please note that filtering by image resolution can take a little while, depending on the number of images in the folder.</source>
      <translation>Зверніть увагу, що фільтрація за роздільною здатністю зображення може зайняти деякий час, залежно від кількості зображень у папці.</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="283"/>
      <location filename="../qml/filter/PQFilterPopout.qml" line="34"/>
      <source>Filter</source>
      <extracomment>Written on a clickable button - please keep short
----------
Window title</extracomment>
      <translation>Фільтр</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="305"/>
      <source>Remove filter</source>
      <extracomment>Written on a clickable button - please keep short</extracomment>
      <translation>Видалити фільтр</translation>
    </message>
  </context>
  <context>
    <name>histogram</name>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="143"/>
      <location filename="../qml/histogram/PQHistogramPopout.qml" line="34"/>
      <source>Histogram</source>
      <extracomment>Window title</extracomment>
      <translation>Гістограма</translation>
    </message>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="145"/>
      <source>Loading...</source>
      <extracomment>As in: Loading the histogram for the current image</extracomment>
      <translation>Завантаження...</translation>
    </message>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="175"/>
      <source>Click-and-drag to move.</source>
      <extracomment>Used for the histogram. The version refers to the type of histogram that is available (colored and greyscale)</extracomment>
      <translation>Натисніть і перетягніть, щоб перемістити.</translation>
    </message>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="175"/>
      <source>Right click to switch version.</source>
      <translation>Правий клац, щоб змінити версію.</translation>
    </message>
  </context>
  <context>
    <name>imageprovider</name>
    <message>
      <location filename="../cplusplus/imageprovider/imageproviderfull.cpp" line="65"/>
      <location filename="../cplusplus/imageprovider/imageproviderthumb.cpp" line="147"/>
      <source>File failed to load, it does not exist!</source>
      <translation>Не вдалося завантажити файл, він не існує!</translation>
    </message>
  </context>
  <context>
    <name>imgur</name>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="86"/>
      <location filename="../qml/imgur/PQImgurPopout.qml" line="34"/>
      <source>Upload to imgur.com</source>
      <extracomment>Window title</extracomment>
      <translation>Завантажити на imgur.com</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="97"/>
      <source>anonymous</source>
      <extracomment>Used as in: Upload image as anonymous user</extracomment>
      <translation>анонімно</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="129"/>
      <source>Obtaining image url...</source>
      <translation>Отримання URL-адреси зображення...</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="140"/>
      <source>This seems to take a long time...</source>
      <translation>Здається, це займе багато часу...</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="141"/>
      <source>There might be a problem with your internet connection or the imgur.com servers.</source>
      <translation>Можливо, виникла проблема з підключенням до Інтернету або серверами imgur.com.</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="152"/>
      <source>An Error occurred while uploading image!</source>
      <translation>Під час завантаження зображення сталася помилка!</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="153"/>
      <source>Error code:</source>
      <translation>Код помилки:</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="164"/>
      <source>You do not seem to be connected to the internet...</source>
      <translation>Здається, ви не підключені до Інтернету...</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="165"/>
      <source>Unable to upload!</source>
      <translation>Неможливо завантажити!</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="187"/>
      <source>Access Image</source>
      <translation>Доступ до зображення</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="200"/>
      <location filename="../qml/imgur/PQImgur.qml" line="232"/>
      <source>Click to open in browser</source>
      <translation>Натисніть, щоб відкрити в браузері</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="207"/>
      <location filename="../qml/imgur/PQImgur.qml" line="239"/>
      <source>Copy to clipboard</source>
      <translation>Копіювати в буфер обміну</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="219"/>
      <source>Delete Image</source>
      <translation>Видалити зображення</translation>
    </message>
  </context>
  <context>
    <name>keymouse</name>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="30"/>
      <source>Alt</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Alt</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="32"/>
      <source>Ctrl</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Ctrl</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="34"/>
      <source>Shift</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Shift</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="36"/>
      <source>Page Up</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Page Up</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="38"/>
      <source>Page Down</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Page Down</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="40"/>
      <source>Meta</source>
      <extracomment>Refers to the key that usually has the Windows symbol on it</extracomment>
      <translation>Мета</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="42"/>
      <source>Keypad</source>
      <extracomment>Refers to the key that triggers the number block on keyboards</extracomment>
      <translation>Клавіатура</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="44"/>
      <source>Escape</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Закрити</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="46"/>
      <source>Right</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Праворуч</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="48"/>
      <source>Left</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Ліворуч</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="50"/>
      <source>Up</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Вгору</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="52"/>
      <source>Down</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Вниз</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="54"/>
      <source>Space</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Пробіл</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="56"/>
      <source>Delete</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Видалити</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="58"/>
      <source>Backspace</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Backspace</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="60"/>
      <source>Home</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Home</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="62"/>
      <source>End</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>End</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="64"/>
      <source>Insert</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Insert</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="66"/>
      <source>Tab</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Tab</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="69"/>
      <source>Return</source>
      <extracomment>Return refers to the enter key of the number block - please try to make the translations of Return and Enter (the main button) different if possible!</extracomment>
      <translation>Return</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="72"/>
      <source>Enter</source>
      <extracomment>Enter refers to the main enter key - please try to make the translations of Return (in the number block) and Enter different if possible!</extracomment>
      <translation>Enter</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="78"/>
      <source>Left Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Ліва кнопка</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="80"/>
      <source>Right Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Права кнопка</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="82"/>
      <source>Middle Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Середня кнопка</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="84"/>
      <source>Double Click</source>
      <extracomment>Refers to a mouse event</extracomment>
      <translation>Подвійне клацання</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="86"/>
      <source>Wheel Up</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation>Колесо вгору</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="88"/>
      <source>Wheel Down</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation>Колесо вниз</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="90"/>
      <source>East</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Схід</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="92"/>
      <source>South</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Південь</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="94"/>
      <source>West</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Захід</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="96"/>
      <source>North</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Північ</translation>
    </message>
  </context>
  <context>
    <name>logging</name>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="33"/>
      <source>Logging</source>
      <extracomment>Window title</extracomment>
      <translation>Журналювання</translation>
    </message>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="109"/>
      <source>enable debug messages</source>
      <translation>увімкнути повідомлення відлагодження</translation>
    </message>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="134"/>
      <source>copy to clipboard</source>
      <translation>копіювати в буфер обміну</translation>
    </message>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="135"/>
      <source>save to file</source>
      <translation>зберегти у файл</translation>
    </message>
  </context>
  <context>
    <name>metadata</name>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="80"/>
      <source>File name</source>
      <extracomment>Please keep string short!</extracomment>
      <translation>Ім'я файлу</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="82"/>
      <source>Dimensions</source>
      <extracomment>The dimensions of the loaded image. Please keep string short!</extracomment>
      <translation>Розміри</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="86"/>
      <source>File size</source>
      <extracomment>Please keep string short!</extracomment>
      <translation>Розмір файлу</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="88"/>
      <source>File type</source>
      <extracomment>Please keep string short!</extracomment>
      <translation>Тип файлу</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="91"/>
      <source>Make</source>
      <extracomment>Exif image metadata: the make of the camera used to take the photo. Please keep string short!</extracomment>
      <translation>Виробник</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="93"/>
      <source>Model</source>
      <extracomment>Exif image metadata: the model of the camera used to take the photo. Please keep string short!</extracomment>
      <translation>Модель</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="95"/>
      <source>Software</source>
      <extracomment>Exif image metadata: the software used to create the photo. Please keep string short!</extracomment>
      <translation>Програмне забезпечення</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="98"/>
      <source>Time Photo was Taken</source>
      <extracomment>Exif image metadata: when the photo was taken. Please keep string short!</extracomment>
      <translation>Час зйомки</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="100"/>
      <source>Exposure Time</source>
      <extracomment>Exif image metadata: how long the sensor was exposed to the light. Please keep string short!</extracomment>
      <translation>Час експозиції</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="102"/>
      <source>Flash</source>
      <extracomment>Exif image metadata: the flash setting when the photo was taken. Please keep string short!</extracomment>
      <translation>Спалах</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="105"/>
      <source>Scene Type</source>
      <extracomment>Exif image metadata: the specific scene type the camera used for the photo. Please keep string short!</extracomment>
      <translation>Тип сцени</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="107"/>
      <source>Focal Length</source>
      <extracomment>Exif image metadata: https://en.wikipedia.org/wiki/Focal_length . Please keep string short!</extracomment>
      <translation>Фокусна відстань</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="150"/>
      <location filename="../qml/menumeta/PQMetaDataPopout.qml" line="34"/>
      <source>Metadata</source>
      <extracomment>This is the heading of the metadata element
----------
Window title</extracomment>
      <translation>Метаданні</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="186"/>
      <source>No File Loaded</source>
      <translation>Файл не завантажено</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="239"/>
      <source>Click to open GPS position with online map</source>
      <translation>Натисніть, щоб відкрити позицію GPS з онлайн-картою</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="111"/>
      <source>Light Source</source>
      <extracomment>Exif image metadata: What type of light the camera detected. Please keep string short!</extracomment>
      <translation>Джерело освітлення</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="84"/>
      <source>Image</source>
      <extracomment>Used as in &quot;Image 3/16&quot;. The numbers (position of image in folder) are added on automatically. Please keep string short!</extracomment>
      <translation>Зображення</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="114"/>
      <source>Keywords</source>
      <extracomment>IPTC image metadata: A description of the image by the user/software. Please keep string short!</extracomment>
      <translation>Ключові слова</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="116"/>
      <source>Location</source>
      <extracomment>IPTC image metadata: The CITY and COUNTRY the imge was taken in. Please keep string short!</extracomment>
      <translation>Розташування</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="118"/>
      <source>Copyright</source>
      <extracomment>IPTC image metadata. Please keep string short!</extracomment>
      <translation>Авторське право</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="121"/>
      <source>GPS Position</source>
      <extracomment>Exif image metadata. Please keep string short!</extracomment>
      <translation>Місцезнаходження за GPS</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="109"/>
      <source>F Number</source>
      <extracomment>Exif image metadata: https://en.wikipedia.org/wiki/F-number . Please keep string short!</extracomment>
      <translation>Діафрагма</translation>
    </message>
  </context>
  <context>
    <name>navigate</name>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="66"/>
      <source>Click and drag to move</source>
      <translation>Натисніть і перетягніть, щоб перемістити</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="98"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="68"/>
      <source>Navigate to previous image in folder</source>
      <translation>Перейти до попереднього зображення в папці</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="126"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="89"/>
      <source>Navigate to next image in folder</source>
      <translation>Перейдіть до наступного зображення в папці</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="151"/>
      <source>Show main menu</source>
      <translation>Показати головне меню</translation>
    </message>
  </context>
  <context>
    <name>other</name>
    <message>
      <location filename="../qml/mainwindow/PQMessage.qml" line="76"/>
      <source>Open a file to start</source>
      <translation>Відкрити файл, щоб почати</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="114"/>
      <source>Click anywhere to open a file</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation>Натисніть в будь-якому місці, щоб відкрити файл</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="124"/>
      <source>Move your cursor to:</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation>Перемістить ваш курсор до:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="134"/>
      <source>RIGHT EDGE for the main menu</source>
      <extracomment>Part of the message shown in the main view before any image is loaded, first option for where to move cursor to</extracomment>
      <translation>ПРАВИЙ КРАЙ для головного меню</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="145"/>
      <source>BOTTOM EDGE to show the thumbnails</source>
      <extracomment>Part of the message shown in the main view before any image is loaded, third option for where to move cursor to</extracomment>
      <translation>НИЖНІЙ КРАЙ, щоб показати мініатюри</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="155"/>
      <source>(once an image/folder is loaded)</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation>(після завантаження зображення/папки)</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="196"/>
      <source>No matches found</source>
      <extracomment>Used as in: No matches found for the currently set filter</extracomment>
      <translation>Збігів не знайдено</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="208"/>
      <source>PhotoQt Image Viewer</source>
      <extracomment>The window title of PhotoQt</extracomment>
      <translation>Переглядач зображень PhotoQt</translation>
    </message>
  </context>
  <context>
    <name>popinpopout</name>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="269"/>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="294"/>
      <location filename="../qml/chromecast/PQChromecast.qml" line="287"/>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="230"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="229"/>
      <location filename="../qml/filemanagement/PQRename.qml" line="212"/>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="344"/>
      <location filename="../qml/filter/PQFilter.qml" line="360"/>
      <location filename="../qml/histogram/PQHistogram.qml" line="297"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="341"/>
      <location filename="../qml/menumeta/PQMetaData.qml" line="290"/>
      <location filename="../qml/scale/PQScale.qml" line="328"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="365"/>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="531"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="402"/>
      <source>Merge into main interface</source>
      <extracomment>Tooltip of small button to merge a popped out element (i.e., one in its own window) into the main interface</extracomment>
      <translation>Об'єднати в основний інтерфейс</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="271"/>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="296"/>
      <location filename="../qml/chromecast/PQChromecast.qml" line="289"/>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="232"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="231"/>
      <location filename="../qml/filemanagement/PQRename.qml" line="214"/>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="346"/>
      <location filename="../qml/filter/PQFilter.qml" line="362"/>
      <location filename="../qml/histogram/PQHistogram.qml" line="299"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="343"/>
      <location filename="../qml/menumeta/PQMetaData.qml" line="292"/>
      <location filename="../qml/scale/PQScale.qml" line="330"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="367"/>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="533"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="404"/>
      <source>Move to its own window</source>
      <extracomment>Tooltip of small button to show an element in its own window (i.e., not merged into main interface)</extracomment>
      <translation>Перенести у власне вікно</translation>
    </message>
  </context>
  <context>
    <name>quickinfo</name>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="117"/>
      <source>Click here to show main menu</source>
      <translation>Натисніть тут, щоб показати головне меню</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="145"/>
      <source>Click here to enter fullscreen mode</source>
      <translation>Натисніть тут, щоб перейти в повноекранний режим</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="146"/>
      <source>Click here to exit fullscreen mode</source>
      <translation>Натисніть тут, щоб вийти з повноекранного режиму</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="183"/>
      <source>Click here to close PhotoQt</source>
      <translation>Натисніть тут, щоб закрити PhotoQt</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="127"/>
      <source>Page %1 of %2</source>
      <extracomment>Used as in: Page 12/34 - please keep as short as possible</extracomment>
      <translation>Сторінка %1 із %2</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="130"/>
      <source>File %1 of %2</source>
      <extracomment>Used as in: File 12/34 - please keep as short as possible</extracomment>
      <translation>Файл %1 із %2</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="221"/>
      <source>Connected to:</source>
      <extracomment>This is followed by the name of the Chromecast streaming device currently connected to</extracomment>
      <translation>Під'єднано до:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="232"/>
      <source>Copy filename to clipboard</source>
      <translation>Копіювати назву файлу в буфер обміну</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="234"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="206"/>
      <source>Show counter</source>
      <translation>Показати лічильник</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="235"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="207"/>
      <source>Hide counter</source>
      <translation>Приховати лічильник</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="237"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="209"/>
      <source>Show file path</source>
      <translation>Показати шлях до файлу</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="238"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="210"/>
      <source>Hide file path</source>
      <translation>Сховати шлях до файлу</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="240"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="212"/>
      <source>Show file name</source>
      <translation>Показати ім'я файлу</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="241"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="213"/>
      <source>Hide file name</source>
      <translation>Приховати ім'я файлу</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="243"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="215"/>
      <source>Show zoom level</source>
      <translation>Показати рівень масштабування</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="244"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="216"/>
      <source>Hide zoom level</source>
      <translation>Приховати рівень масштабування</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQImage.qml" line="701"/>
      <location filename="../qml/mainwindow/PQLabels.qml" line="356"/>
      <source>Click here to enter viewer mode</source>
      <translation>Натисніть тут, щоб увійти в режим перегляду</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQImage.qml" line="720"/>
      <location filename="../qml/mainwindow/PQLabels.qml" line="329"/>
      <source>Hide central &apos;viewer mode&apos; button</source>
      <translation>Приховати кнопку центрального режиму перегляду</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="356"/>
      <source>Click here to exit viewer mode</source>
      <translation>Натисніть тут, щоб вийти з режиму перегляду</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="199"/>
      <source>Filter:</source>
      <translation>Фільтр:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="246"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="218"/>
      <source>Show window buttons</source>
      <translation>Показувати кнопки вікна</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="247"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="219"/>
      <source>Hide window buttons</source>
      <translation>Сховати кнопки вікна</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="286"/>
      <source>Some info about the current image and directory</source>
      <translation>Деяка інформація про поточне зображення та каталог</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="330"/>
      <source>Show central &apos;viewer mode&apos; button</source>
      <translation>Показати центральну кнопку режиму перегляду</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQLabels.qml" line="404"/>
      <source>Click to remove filter</source>
      <translation>Натисніть, щоб видалити фільтр</translation>
    </message>
  </context>
  <context>
    <name>scale</name>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="84"/>
      <location filename="../qml/scale/PQScalePopout.qml" line="34"/>
      <source>Scale file</source>
      <extracomment>Window title</extracomment>
      <translation>Масштабувати файл</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="94"/>
      <source>An error occured, file could not be scaled!</source>
      <translation>Сталася помилка, файл не вдалося масштабувати!</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="104"/>
      <source>This file format cannot (yet) be scaled with PhotoQt!</source>
      <translation>Цей формат файлу (ще) не можна масштабувати за допомогою PhotoQt!</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="115"/>
      <source>New width x height:</source>
      <translation>Нова ширина та висота:</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="188"/>
      <source>Preserve aspect ratio</source>
      <extracomment>The aspect ratio refers to the ratio of the width to the height of the image, e.g., 16:9 for most movies</extracomment>
      <translation>Зберегти співвідношення сторін</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="199"/>
      <source>Quality:</source>
      <extracomment>This refers to the quality to be used to scale the image</extracomment>
      <translation>Якість:</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="232"/>
      <location filename="../qml/scale/PQScale.qml" line="302"/>
      <source>Scale (create new file)</source>
      <extracomment>Written on a clickable button</extracomment>
      <translation>Масштаб (створити новий файл)</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="260"/>
      <location filename="../qml/scale/PQScale.qml" line="303"/>
      <source>Scale (change file in place)</source>
      <extracomment>Written on a clickable button</extracomment>
      <translation>Масштаб (змінити файл на місці)</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="304"/>
      <source>De-/Increase width and height by 10%</source>
      <translation>Зменшити/збільшити ширину та висоту на 10%</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="305"/>
      <source>In-/Decrease quality by 5%</source>
      <translation>Збільшити/зменшити якість на 5%</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/handlingmanipulation.cpp" line="233"/>
      <source>Select new file</source>
      <translation>Вибрати новий файл</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager</name>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="100"/>
      <source>interface</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>інтерфейс</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="101"/>
      <source>Tab to control interface settings</source>
      <translation>Вкладка для керування налаштуваннями інтерфейсу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="103"/>
      <source>image view</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>Перегляд зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="104"/>
      <source>Tab to control how images are viewed</source>
      <translation>Вкладка для керування переглядом зображень</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="106"/>
      <source>thumbnails</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>мініатюри</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="107"/>
      <source>Tab to control the look and behaviour of thumbnails</source>
      <translation>Вкладка для керування виглядом та поведінкою мініатюр</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="109"/>
      <source>metadata</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>метаданні</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="110"/>
      <source>Tab to control metadata settings</source>
      <translation>Вкладка для керування параметрами метаданих</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="112"/>
      <source>file types</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>типи файлів</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="113"/>
      <source>Tab to control which file types PhotoQt should recognize</source>
      <translation>Вкладка для керування типами файлів, які має розпізнавати PhotoQt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="115"/>
      <source>shortcuts</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>гарячі клавіші</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="116"/>
      <source>Tab to control which shortcuts are set</source>
      <translation>Вкладка для керування гарячими клавішами</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="224"/>
      <source>advanced</source>
      <extracomment>Written on button in setting manager. A click on this button opens a menu with some advanced actions.</extracomment>
      <translation>розширений</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="228"/>
      <source>restore defaults</source>
      <translation>відновити значення за замовчуванням</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="229"/>
      <source>import settings</source>
      <translation>імпортувати налаштування</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="230"/>
      <source>export settings</source>
      <translation>експорт налаштувань</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="231"/>
      <source>disable expert mode</source>
      <translation>вимкнути режим експерта</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="231"/>
      <source>enable expert mode</source>
      <translation>увімкнути режим експерта</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="271"/>
      <source>Import of %1. This will replace your current settings with the ones stored in the backup.</source>
      <translation>Імпорт %1. Це замінить ваші поточні налаштування на ті, що зберігаються в резервній копії.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="272"/>
      <source>Do you want to continue?</source>
      <translation>Бажаєте продовжити?</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="324"/>
      <source>Save changes and exit</source>
      <translation>Зберегти зміни та вийти</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="335"/>
      <source>Exit and discard changes</source>
      <translation>Вийти та скасувати зміни</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="175"/>
      <source>Rename File</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Перейменувати файл</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="177"/>
      <source>Delete File</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Видалити файл</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabFileTypes.qml" line="76"/>
      <source>Filetype settings</source>
      <translation>Параметри типу файлу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabFileTypes.qml" line="90"/>
      <source>These settings govern which file types PhotoQt should recognize and open.</source>
      <translation>Ці параметри визначають, які типи файлів PhotoQt має розпізнавати та відкривати.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabFileTypes.qml" line="90"/>
      <source>Not all file types might be available, depending on your setup and what library support was enabled at compile time</source>
      <translation>Не всі типи файлів можуть бути доступними, залежно від ваших налаштувань і того, яку підтримку бібліотеки було ввімкнено під час компіляції</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabImageView.qml" line="84"/>
      <source>Image view settings</source>
      <translation>Налаштування перегляду зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabImageView.qml" line="98"/>
      <source>These settings affect the viewing of images, how they are shown, in what order, how large a cache to use, etc.</source>
      <translation>Ці параметри впливають на перегляд зображень, спосіб їх показу, порядок, розмір кешу, тощо.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabImageView.qml" line="98"/>
      <location filename="../qml/settingsmanager/tabs/PQTabInterface.qml" line="100"/>
      <location filename="../qml/settingsmanager/tabs/PQTabMetadata.qml" line="99"/>
      <location filename="../qml/settingsmanager/tabs/PQTabThumbnails.qml" line="99"/>
      <source>Some settings are only shown in expert mode.</source>
      <translation>Деякі налаштування показуються лише в експертному режимі.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabInterface.qml" line="86"/>
      <source>Interface settings</source>
      <translation>Налаштування інтерфейсу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabInterface.qml" line="100"/>
      <source>These settings affect the interface in general, how the application looks like and behaves. This includes the background, some of the labels in the main view, which elements are to be shown in their own window, and others.</source>
      <translation>Ці налаштування впливають на інтерфейс загалом, на те, як програма виглядає та поводиться. Це включає фон, деякі ярлики в головному вікні, елементи, які мають показуватися в окремому вікні, та інше.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabMetadata.qml" line="85"/>
      <source>Metadata settings</source>
      <translation>Параметри метаданих</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabMetadata.qml" line="99"/>
      <source>These settings affect the metadata element, what information it should show and some of its behavior.</source>
      <translation>Ці параметри впливають на елемент метаданих, інформацію, яку він має показувати, і певну його поведінку.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="77"/>
      <source>Shortcuts</source>
      <translation>Гарячі клавіші</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="101"/>
      <source>Navigation</source>
      <extracomment>A shortcuts category: navigation</extracomment>
      <translation>Навігація</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="107"/>
      <source>Filter images in folder</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Фільтрувати зображення в папці</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="109"/>
      <source>Next image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Наступне зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="111"/>
      <source>Previous image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Попереднє зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="115"/>
      <source>Go to first image</source>
      <extracomment>Name of shortcut action Name of shortcut action</extracomment>
      <translation>Перейти до першого зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="117"/>
      <source>Go to last image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Перейти до останнього зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="119"/>
      <source>Enter viewer mode</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Перейти у режим перегляду</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="123"/>
      <source>Close window (hides to system tray if enabled)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Закрити вікно (приховується в системному треї, якщо увімкнено)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="125"/>
      <source>Quit PhotoQt</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Вийти з PhotoQt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="133"/>
      <source>Image</source>
      <extracomment>A shortcuts category: image manipulation</extracomment>
      <translation>Зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="137"/>
      <source>Zoom In</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Збільшити</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="139"/>
      <source>Zoom Out</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Зменшити</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="141"/>
      <source>Zoom to Actual Size</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Збільшити для фактичного розміру</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="143"/>
      <source>Reset Zoom</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Скинути масштаб</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="145"/>
      <source>Rotate Right</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Повернути праворуч</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="147"/>
      <source>Rotate Left</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Повернути ліворуч</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="149"/>
      <source>Reset Rotation</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Скинути обертання</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="151"/>
      <source>Flip Horizontally</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Віддзеркалити горизонтально</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="153"/>
      <source>Flip Vertically</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Віддзеркалити вертикально</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="155"/>
      <source>Scale Image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Масштабувати зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="157"/>
      <source>Play/Pause animation/video</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Відтворення/пауза анімації/відео</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="159"/>
      <source>Hide/Show face tags (stored in metadata)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Сховати/показати теги обличчя (зберігаються в метаданих)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="163"/>
      <source>Advanced image sort (Quickstart)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Розширене сортування зображень (Швидкий старт)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="211"/>
      <source>Start Slideshow (Setup)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Почати слайд-шоу (налаштування)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="171"/>
      <source>File</source>
      <extracomment>A shortcuts category: file management</extracomment>
      <translation>Файл</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="121"/>
      <source>Show floating navigation buttons</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Показувати плавучі кнопки навігації</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="179"/>
      <source>Delete File (without confirmation)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Видалити файл (без підтвердження)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="181"/>
      <source>Copy File to a New Location</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Скопіювати файл у нове розташування</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="183"/>
      <source>Move File to a New Location</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Перемістити файл у нове розташування</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="185"/>
      <source>Copy Image to Clipboard</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Копіювати зображення в буфер обміну</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="189"/>
      <source>Print current photo</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Видрукувати поточне фото</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="197"/>
      <source>Other</source>
      <extracomment>A shortcuts category: other functions</extracomment>
      <translation>Інше</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="201"/>
      <source>Hide/Show main menu</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Сховати/показати головне меню</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="203"/>
      <source>Hide/Show metadata</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Сховати/показати метадані</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="205"/>
      <source>Keep metadata opened</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Залишити метадані відкритими</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="207"/>
      <source>Hide/Show thumbnails</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Сховати/показати мініатюри</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="209"/>
      <source>Show Settings</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Показати налаштування</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="213"/>
      <source>Start Slideshow (Quickstart)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Почати слайд-шоу (Швидкий старт)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="215"/>
      <source>About PhotoQt</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Про PhotoQt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="217"/>
      <source>Set as Wallpaper</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Встановити як шпалери</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="219"/>
      <source>Show Histogram</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Показати гістограму</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="221"/>
      <source>Upload to imgur.com (anonymously)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Вивантажити на imgur.com (анонімно)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="223"/>
      <source>Upload to imgur.com user account</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Вивантажити на обліковий запис користувача imgur.com</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="225"/>
      <source>Stream content to Chromecast device</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Потокова передача на пристрій Chromecast</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="227"/>
      <source>Show log/debug messages</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Показати повідомлення журналу/налагодження</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="237"/>
      <source>External</source>
      <extracomment>A shortcuts category: external shortcuts</extracomment>
      <translation>Зовнішній</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="240"/>
      <source>%f = filename including path, %u = filename without path, %d = directory containing file</source>
      <extracomment>Please leave the three placeholders (%f, %u, %d) as is.</extracomment>
      <translation>%f = ім'я файлу, включаючи шлях, %u = ім'я файлу без шляху, %d = каталог, що містить файл</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabThumbnails.qml" line="85"/>
      <source>Thumbnails settings</source>
      <translation>Налаштування мініатюр</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabThumbnails.qml" line="99"/>
      <source>These settings affect the thumbnails shown, by default, along the bottom edge of the screen. This includes their look, behavior, and the user&apos;s interaction with them.</source>
      <translation>Ці налаштування впливають на мініатюри, які за замовчуванням показуються вздовж нижнього краю екрана. Це включає їхній вигляд, поведінку та взаємодію користувача з ними.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManagerPopout.qml" line="34"/>
      <source>Settings Manager</source>
      <extracomment>Window title</extracomment>
      <translation>Менеджер налаштувань</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="91"/>
      <source>Here the shortcuts can be managed. Below you can add a new shortcut for any one of the available actions, both key combinations and mouse gestures are supported.</source>
      <translation>Тут можна керувати гарячими клавішами. Нижче ви можете додати нові гарячі клавіші для будь-якої з доступних дій, підтримуються як комбінації клавіш, так і жести миші.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="91"/>
      <source>You can also set the same shortcut for multiple actions or multiple times for the same action. All actions for a shortcut will be executed sequentially, allowing a lot more flexibility in using PhotoQt.</source>
      <translation>Ви також можете встановити ті самі гарячі клавіші для кількох дій або кілька разів для однієї дії. Усі дії для гарячих клавіш виконуватимуться послідовно, що забезпечує набагато більшу гнучкість у використанні PhotoQt.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="105"/>
      <source>Open file (browse images)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Відкрити файл (переглянути зображення)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="161"/>
      <source>Advanced image sort (Setup)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Розширене сортування зображень (Налаштування)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="187"/>
      <source>Save image in another format</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Зберегти зображення в іншому форматі</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="72"/>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="128"/>
      <source>Restore defaults</source>
      <extracomment>As in &apos;restore the default settings and/or file formats and/or shortcuts&apos;. Please keep short!</extracomment>
      <translation>Відновити значення за замовчуванням</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="83"/>
      <source>Here you can restore the default configuration of PhotoQt. You can choose to restore any combination of the following three categories.</source>
      <translation>Тут Ви можете відновити стандартну конфігурацію PhotoQt. Ви можете відновити будь-яку комбінацію з наступних трьох категорій.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="96"/>
      <source>Restore default settings</source>
      <translation>Відновити налаштування за замовчуванням</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="103"/>
      <source>Restore default file formats</source>
      <translation>Відновити стандартні формати файлів</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="109"/>
      <source>Restore default shortcuts</source>
      <translation>Відновити стандартні гарячі клавіші</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_filetypes</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQLibArchive.qml" line="32"/>
      <source>These are some additional settings for opening archives.</source>
      <translation>Це деякі додаткові параметри для відкриття архівів.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQLibArchive.qml" line="45"/>
      <source>use external tool: unrar</source>
      <extracomment>used as label for checkbox</extracomment>
      <translation>використовувати зовнішній інструмент: unrar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQPoppler.qml" line="32"/>
      <source>These are some additional settings for showing PDFs.</source>
      <translation>Це деякі додаткові параметри для показу PDF.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQPoppler.qml" line="44"/>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQPoppler.qml" line="55"/>
      <source>Quality:</source>
      <extracomment>the quality setting to be used when loading PDFs</extracomment>
      <translation>Якість:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="32"/>
      <source>These are some additional settings for playing videos.</source>
      <translation>Це деякі додаткові налаштування для відтворення відео.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="45"/>
      <source>Autoplay</source>
      <extracomment>Used as setting for video files (i.e., autoplay videos)</extracomment>
      <translation>Автоматичне відтворення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="51"/>
      <source>Loop</source>
      <extracomment>Used as setting for video files (i.e., loop videos)</extracomment>
      <translation>Повторення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="58"/>
      <source>Prefer libmpv</source>
      <extracomment>Used as setting for video files</extracomment>
      <translation>Надавати перевагу libmpv</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="59"/>
      <source>libmpv can offer a more comprehensive video support but tends to be slightly slower to load.</source>
      <translation>libmpv може запропонувати більш комплексну підтримку відео, але завантаження може бути трохи повільнішим.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="64"/>
      <source>Select tool for creating video thumbnails</source>
      <extracomment>Tooltip shown for combobox for selectiong video thumbnailer</extracomment>
      <translation>Виберіть інструмент для створення мініатюр відео</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="55"/>
      <source>images</source>
      <extracomment>This is a category of files PhotoQt can recognize: any image format</extracomment>
      <translation>зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="57"/>
      <source>compressed files</source>
      <extracomment>This is a category of files PhotoQt can recognize: compressed files like zip, tar, cbr, 7z, etc.</extracomment>
      <translation>стиснуті файли</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="59"/>
      <source>documents</source>
      <extracomment>This is a category of files PhotoQt can recognize: documents like pdf, txt, etc.</extracomment>
      <translation>документи</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="61"/>
      <source>videos</source>
      <extracomment>This is a type of category of files PhotoQt can recognize: videos like mp4, avi, etc.</extracomment>
      <translation>відео</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="67"/>
      <source>Enable</source>
      <extracomment>As in: &quot;Enable all formats in the seleted category of file types&quot;</extracomment>
      <translation>Увімкнути</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="73"/>
      <source>Disable</source>
      <extracomment>As in: &quot;Disable all formats in the seleted category of file types&quot;</extracomment>
      <translation>Вимкнути</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="99"/>
      <source>Enable everything</source>
      <extracomment>As in &quot;Enable every single file format PhotoQt can open in any category&quot;</extracomment>
      <translation>Увімкнути все</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="117"/>
      <source>Currently there are %1 file formats enabled</source>
      <extracomment>The %1 will be replaced with the number of file formats, please don&apos;t forget to add it.</extracomment>
      <translation>Наразі ввімкнено %1 форматів файлів</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="145"/>
      <source>Search by description or file ending</source>
      <translation>Пошук за описом або закінченням файлу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="151"/>
      <source>Search by image library or category</source>
      <translation>Пошук за бібліотекою зображень або категорією</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="240"/>
      <source>File endings:</source>
      <translation>Закінчення файлів:</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_imageview</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="31"/>
      <source>animation</source>
      <extracomment>A settings title referring to the in/out animation of images</extracomment>
      <translation>анімація</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="33"/>
      <source>What type of animation to show, and how fast.</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Який тип анімації показувати та як швидко.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="44"/>
      <source>type of animation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>тип анімації</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="47"/>
      <source>opacity</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>непрозорість</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="49"/>
      <source>along x-axis</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>вздовж осі х</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="51"/>
      <source>along y-axis</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>вздовж осі у</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="53"/>
      <source>rotation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>обертання</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="55"/>
      <source>explosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>вибух</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="57"/>
      <source>implosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>імплозія</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="59"/>
      <source>choose one at random</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>виберіть один навмання</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="76"/>
      <source>no animation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>без анімації</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="91"/>
      <source>long animation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>довга анімація</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQFitInWindow.qml" line="30"/>
      <source>fit in window</source>
      <extracomment>A settings title referring to whether to fit images in window</extracomment>
      <translation>вписати у вікно</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQFitInWindow.qml" line="31"/>
      <source>Zoom smaller images to fill the full window width and/or height.</source>
      <translation>Масштабувати менші зображення, щоб заповнити всю ширину та/або висоту вікна.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQFitInWindow.qml" line="36"/>
      <source>fit smaller images in window</source>
      <translation>розмістити менші зображення у вікні</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="30"/>
      <source>interpolation</source>
      <extracomment>A settings title referring to the type of interpolation to use for small images</extracomment>
      <translation>інтерполяція</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="31"/>
      <source>PhotoQt tries to improve the rendering of images that are shown much larger than they are (i.e., zoomed in a lot). For very tiny images that are zoomed in quite a lot, this can result in the loss of too much information in the image. Thus a threshold can be defined here, images that are smaller than this threshold are shown exactly as they are without any smoothing or other attempts to improve them.</source>
      <translation>PhotoQt намагається покращити візуалізацію зображень, які відображаються набагато більшими, ніж вони є (тобто сильно збільшені). Для дуже маленьких зображень, які досить збільшені, це може призвести до втрати занадто великої кількості інформації на зображенні. Таким чином, тут можна визначити порогове значення, зображення, менші за це порогове значення, відображаються точно такими, якими вони є, без будь-якого згладжування чи інших спроб їх покращити.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="42"/>
      <source>Do not use any interpolation algorithm for very small images</source>
      <extracomment>A type of interpolation to use for small images</extracomment>
      <translation>Не використовувати будь-який алгоритм інтерполяції для дуже малих зображень</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="53"/>
      <source>threshold:</source>
      <extracomment>The threshold (in pixels) at which to switch interpolation algorithm</extracomment>
      <translation>поріг:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQKeep.qml" line="30"/>
      <source>remember per session</source>
      <extracomment>A settings title</extracomment>
      <translation>пам’ятати на сеанс</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQKeep.qml" line="31"/>
      <source>By default, PhotoQt resets the zoom, rotation, flipping/mirroring and position when switching to a different image. For certain tasks, for example for comparing two images, it can be helpful to keep these properties.</source>
      <translation>За замовчуванням PhotoQt скидає масштаб, обертання, віддзеркалення та положення під час переходу на інше зображення. Для певних завдань, наприклад для порівняння двох зображень, може бути корисним зберегти ці властивості.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQKeep.qml" line="37"/>
      <source>remember zoom, rotation, flip, position</source>
      <translation>запам'ятати масштабування, обертання, поворот, положення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQLoop.qml" line="30"/>
      <source>looping</source>
      <extracomment>A settings title for looping through images in folder</extracomment>
      <translation>зациклювання</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQLoop.qml" line="31"/>
      <source>What to do when the end of a folder has been reached: stop or loop back to first image in folder.</source>
      <translation>Що робити, коли досягнуто кінця папки: зупинити або повернутися до першого зображення в папці.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQLoop.qml" line="36"/>
      <source>loop through images in folder</source>
      <translation>цикл через зображення в папці</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQMargin.qml" line="30"/>
      <source>margin</source>
      <extracomment>A settings title about the margin around the main image</extracomment>
      <translation>відступ</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQMargin.qml" line="31"/>
      <source>How much space to show between the main image and the application border.</source>
      <translation>Скільки місця показати між основним зображенням і межею програми.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQMargin.qml" line="44"/>
      <source>none</source>
      <extracomment>As in: no margin between the main image and the window edges</extracomment>
      <translation>нічого</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQPixmapCache.qml" line="30"/>
      <source>pixmap cache</source>
      <extracomment>A settings title</extracomment>
      <translation>кеш піксельної карти</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQPixmapCache.qml" line="31"/>
      <source>Size of runtime cache for fully loaded images. This cache is cleared when the application quits.</source>
      <translation>Розмір кешу виконання для повністю завантажених зображень. Цей кеш очищається, коли програма закривається.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="31"/>
      <source>sort images by</source>
      <extracomment>A settings title</extracomment>
      <translation>сортувати зображення за</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="32"/>
      <source>Sort all images in a folder by the set property.</source>
      <translation>Сортувати всі зображення в папці за встановленою властивістю.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="43"/>
      <source>natural name</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>природна назва</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="45"/>
      <source>name</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>ім’я</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="47"/>
      <source>time</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>час</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="49"/>
      <source>size</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>розмір</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="51"/>
      <source>type</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>тип</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="58"/>
      <source>ascending</source>
      <extracomment>Sort images in ascending order</extracomment>
      <translation>за зростанням</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="65"/>
      <source>descending</source>
      <extracomment>Sort images in descending order</extracomment>
      <translation>за спаданням</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQTransparencyMarker.qml" line="30"/>
      <source>transparency marker</source>
      <extracomment>A settings title</extracomment>
      <translation>маркер прозорості</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQTransparencyMarker.qml" line="31"/>
      <source>Show checkerboard pattern behind transparent areas of (half-)transparent images.</source>
      <translation>Показувати шаховий порядок за прозорими областями (напів)прозорих зображень.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQTransparencyMarker.qml" line="37"/>
      <source>show checkerboard pattern</source>
      <extracomment>Setting for how to display images that have transparent areas, whether to show checkerboard pattern in that area or not</extracomment>
      <translation>показати шаховий порядок</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="30"/>
      <source>zoom speed</source>
      <extracomment>A settings title, the zoom here is the zoom of the main image</extracomment>
      <translation>швидкість масштабування</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="31"/>
      <source>Images are zoomed at a relative speed as specified by this percentage. A higher value means faster zoom.</source>
      <translation>Зображення масштабуються з відносною швидкістю, визначеною цим відсотком. Більше значення означає швидше масштабування.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="43"/>
      <source>super slow</source>
      <extracomment>This refers to the zoom speed, the zoom here is the zoom of the main image</extracomment>
      <translation>надповільно</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="58"/>
      <source>very fast</source>
      <extracomment>This refers to the zoom speed, the zoom here is the zoom of the main image</extracomment>
      <translation>дуже швидко</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="31"/>
      <source>zoom to/from</source>
      <extracomment>A settings title, the zoom here is the zoom of the main image</extracomment>
      <translation>збільшити до/від</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="32"/>
      <source>This controls whether the image is zoomed to/from the mouse position or to/from the image center. Note that this only applies when zooming by mouse, zooming by keyboard shortcut always zooms to/from the image center.</source>
      <translation>Це визначає, чи буде масштабовано зображення до/від позиції миші чи до/від центру зображення. Зверніть увагу, що це стосується лише масштабування за допомогою миші, масштабування за допомогою комбінації клавіш завжди масштабує до/від центру зображення.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="42"/>
      <source>mouse position</source>
      <translation>положення миші</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="47"/>
      <source>image center</source>
      <translation>центр зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="30"/>
      <source>zoom min/max</source>
      <extracomment>A settings title, the zoom here is the zoom of the main image</extracomment>
      <translation>масштаб мін/макс</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="31"/>
      <source>This defines the minimum and/or maximum zoom level for an image.</source>
      <translation>Це визначає мінімальний та/або максимальний рівень масштабування для зображення.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="31"/>
      <source>Note that the maximum zoom level is the absolute zoom level, the minimum zoom level is relative to the default zoom level (the zoom level when the image is first loaded).</source>
      <translation>Зауважте, що максимальний рівень масштабування є абсолютним рівнем масштабування, мінімальний рівень масштабування – відносно рівня масштабування за замовчуванням (рівень масштабування під час першого завантаження зображення).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="52"/>
      <source>minimum zoom:</source>
      <translation>мінімальний масштаб:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="91"/>
      <source>maximum zoom:</source>
      <translation>максимальний масштаб:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQDoubleClick.qml" line="30"/>
      <source>Double Click Threshold</source>
      <extracomment>A settings title</extracomment>
      <translation>Поріг подвійного клацання</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQDoubleClick.qml" line="31"/>
      <source>Two clicks within the specified interval are interpreted as a double click. Note that too high a value will cause noticable delays in reacting to single clicks.</source>
      <translation>Два клацання в межах зазначеного інтервалу інтерпретуються як подвійне клацання. Зауважте, що занадто високе значення призведе до помітних затримок у реакції на окремі клацання.</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_interface</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="32"/>
      <source>background</source>
      <extracomment>A settings title referring to the background of PhotoQt (behind any image/element)</extracomment>
      <translation>фон</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="34"/>
      <source>What type of background is to be shown.</source>
      <extracomment>The background here refers to the area behind the main image and any element in PhotoQt, the very back.</extracomment>
      <translation>Тип фону для показу.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="45"/>
      <source>(half-)transparent background</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>(напів)прозорий фон</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="47"/>
      <source>faked transparency</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>несправжня прозорість</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="49"/>
      <source>custom background image</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>користувацьке фонове зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="76"/>
      <source>Click to select an image</source>
      <extracomment>Tooltip for a mouse area, a click on which opens a file dialog for selecting an image</extracomment>
      <translation>Натисніть для вибору зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="87"/>
      <source>scale to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>підігнати розмір</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="89"/>
      <source>scale and crop to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>масштабувати та кадрувати</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="91"/>
      <source>stretch to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>розтягнути</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="93"/>
      <source>center image</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>зображення в центрі</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="95"/>
      <source>tile image</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>плиткою</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="201"/>
      <source>All Images</source>
      <extracomment>This is a category in a file dialog for selecting images used as in: All images supported by PhotoQt.</extracomment>
      <translation>Усі зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="216"/>
      <source>Video</source>
      <extracomment>This is a category in a file dialog for selecting images used as in: Video files supported by PhotoQt.</extracomment>
      <translation>Відео</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="31"/>
      <source>empty area around image</source>
      <extracomment>A settings title</extracomment>
      <translation>порожня область навколо зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="32"/>
      <source>How to handle clicks on empty area around images.</source>
      <translation>Як обробляти клацання порожньої області навколо зображень.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="40"/>
      <source>close on click</source>
      <extracomment>Used as in: Close PhotoQt on click on empty area around main image</extracomment>
      <translation>закрити по клацанню</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="41"/>
      <source>Close PhotoQt when click occurred on empty area around image</source>
      <translation>Закрити PhotoQt при натисканні на порожню область навколо зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="52"/>
      <source>navigate on click</source>
      <extracomment>Used as in: Navigate in folder on click on empty area around main image</extracomment>
      <translation>навігація за клацанням</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="53"/>
      <source>Go to next/previous image if click occurred in left/right half of window</source>
      <translation>Перейти до наступного/попереднього зображення, якщо натиснути в лівій/правій половині вікна</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="63"/>
      <source>toggle window decoration</source>
      <translation>перемкнути декорування вікна</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="64"/>
      <source>Toggle window decoration</source>
      <translation>Перемкнути декорування вікна</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="30"/>
      <source>size of &apos;hot edge&apos;</source>
      <extracomment>A settings title. The hot edge refers to the area along the edges of PhotoQt where the mouse cursor triggers an action (e.g., showing the thumbnails or the main menu)</extracomment>
      <translation>розмір гарячого краю</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="31"/>
      <source>Adjusts the sensitivity of the edges for showing elements like the metadata and main menu elements.</source>
      <translation>Регулює чутливість країв для показу таких елементів, як метадані та елементи головного меню.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="40"/>
      <source>small</source>
      <extracomment>used as in: small area</extracomment>
      <translation>маленька</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="57"/>
      <source>large</source>
      <extracomment>used as in: large area</extracomment>
      <translation>велика</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLanguage.qml" line="33"/>
      <source>language</source>
      <extracomment>A settings title.</extracomment>
      <translation>мова</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLanguage.qml" line="34"/>
      <source>Change the language of the application.</source>
      <translation>Змінити мову програми.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="30"/>
      <source>mouse wheel sensitivity</source>
      <extracomment>A settings title.</extracomment>
      <translation>чутливість колеса миші</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="31"/>
      <source>How sensitive the mouse wheel is for shortcuts, etc.</source>
      <translation>Наскільки коліщатко миші чутливе до гарячих клавіш, тощо.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="58"/>
      <source>not sensitive</source>
      <extracomment>The sensitivity here refers to the sensitivity of the mouse wheel</extracomment>
      <translation>не чутливий</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="42"/>
      <source>very sensitive</source>
      <extracomment>The sensitivity here refers to the sensitivity of the mouse wheel</extracomment>
      <translation>дуже чутливий</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="32"/>
      <source>overlay color</source>
      <extracomment>A settings title.</extracomment>
      <translation>колір накладання</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="33"/>
      <source>This is the color that is shown in the background on top of any background image/etc.</source>
      <translation>Це колір, який показується на фоні поверх будь-якого фонового зображення тощо.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="77"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="148"/>
      <source>click to change color</source>
      <translation>натисніть, щоб змінити колір</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="122"/>
      <source>fullscreen mode</source>
      <translation>повноекранний режим</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="186"/>
      <source>use same color in window and fullscreen mode</source>
      <translation>використовувати той самий колір у віконному та повноекранному режимах</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="195"/>
      <source>please choose a color</source>
      <translation>будь ласка оберіть колір</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="31"/>
      <source>pop out elements</source>
      <extracomment>A settings title. The popping out that is talked about here refers to the possibility of showing any element in its own window (i.e., popped out).</extracomment>
      <translation>висувні елементи</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="32"/>
      <source>Here you can choose for most elements whether they are to be shown integrated into the main window or in their own, separate window.</source>
      <translation>Тут ви можете вибрати для більшості елементів, чи будуть вони показуватися в інтегрованому вікні чи в окремому вікні.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="35"/>
      <source>File dialog</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Файловий діалог</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="35"/>
      <source>keep open</source>
      <translation>тримати відкритим</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="37"/>
      <source>Settings Manager</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Менеджер налаштувань</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="39"/>
      <source>Main Menu</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Головне меню</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="41"/>
      <source>Metadata</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Метаданні</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="43"/>
      <source>Histogram</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Гістограма</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="45"/>
      <source>Scale</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Масштаб</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="47"/>
      <source>Slideshow Settings</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Налаштування слайд-шоу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="49"/>
      <source>Slideshow Controls</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Керування слайд-шоу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="51"/>
      <source>Rename File</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Перейменувати файл</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="53"/>
      <source>Delete File</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Видалити файл</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="55"/>
      <source>Save File As</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Зберегти файл як</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="57"/>
      <source>About</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Про</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="59"/>
      <source>Imgur</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Imgur</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="61"/>
      <source>Wallpaper</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Шпалери</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="63"/>
      <source>Filter</source>
      <extracomment>Noun, not a verb. Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Фільтр</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="65"/>
      <source>Advanced Image Sort</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Розширене сортування зображень</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLabels.qml" line="31"/>
      <source>labels</source>
      <extracomment>A settings title.</extracomment>
      <translation>ярлики</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLabels.qml" line="32"/>
      <source>The labels are shown along the top edge of the main view.</source>
      <translation>Ярлики показуються вздовж верхнього краю головного вигляду.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLabels.qml" line="38"/>
      <source>show labels</source>
      <extracomment>checkbox in settings manager</extracomment>
      <translation>показати ярлики</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLabels.qml" line="82"/>
      <source>counter</source>
      <extracomment>refers to the image counter (i.e., image #/# in current folder)</extracomment>
      <translation>лічильник</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLabels.qml" line="95"/>
      <source>filepath</source>
      <extracomment>show filepath in the labels. This is specifically the filePATH and not the filename.</extracomment>
      <translation>шлях до файлу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLabels.qml" line="107"/>
      <source>filename</source>
      <extracomment>show filename in the labels. This is specifically the fileNAME and not the filepath.</extracomment>
      <translation>ім'я файлу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLabels.qml" line="118"/>
      <source>current zoom level</source>
      <translation>поточний рівень масштабування</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLabels.qml" line="129"/>
      <source>current rotation angle</source>
      <translation>поточний кут повороту</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLabels.qml" line="141"/>
      <source>window buttons</source>
      <extracomment>the window buttons are some window management buttons like: close window, maximize, fullscreen</extracomment>
      <translation>віконні кнопки</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLabels.qml" line="153"/>
      <source>always show &apos;x&apos;</source>
      <extracomment>The &apos;x&apos; is a small button (part of the window buttons) that closes the window</extracomment>
      <translation>завжди показувати х</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLabels.qml" line="169"/>
      <source>size of window buttons</source>
      <extracomment>the size of the window buttons (the buttons shown in the top right corner of the window)</extracomment>
      <translation>розмір кнопок вікна</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStartupLoadLast.qml" line="30"/>
      <source>remember last image</source>
      <extracomment>A settings title.</extracomment>
      <translation>запам'ятати останнє зображення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStartupLoadLast.qml" line="31"/>
      <source>At startup the image loaded at the end of the last session can be automatically reloaded.</source>
      <translation>Під час запуску зображення, завантажене в кінці останнього сеансу, може автоматично перезавантажуватися.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStartupLoadLast.qml" line="35"/>
      <source>re-open last loaded image at startup</source>
      <translation>повторно відкрити останнє завантажене зображення при запуску</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="30"/>
      <source>tray icon</source>
      <extracomment>A settings title.</extracomment>
      <translation>іконка в треї</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="31"/>
      <source>If a tray icon is to be shown and, if shown, whether to hide it or not.</source>
      <translation>Якщо піктограма трея має відображатися, і якщо вона відображається, чи приховувати її чи ні.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="36"/>
      <source>no tray icon</source>
      <translation>без іконки в треї</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="37"/>
      <source>hide to tray icon</source>
      <translation>сховати в треї значок</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="38"/>
      <source>show tray icon but don&apos;t hide to it</source>
      <translation>показувати піктограму в треї, але не приховувати її</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="31"/>
      <source>window management</source>
      <extracomment>A settings title.</extracomment>
      <translation>керування вікнами</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="32"/>
      <source>Some basic window management properties.</source>
      <translation>Деякі основні властивості керування вікном.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="40"/>
      <source>manage window through quick info labels</source>
      <translation>керувати вікном через швидкі інформаційні ярлики</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="46"/>
      <source>save and restore window geometry</source>
      <translation>зберегти та відновити віконну геометрію</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="51"/>
      <source>keep above other windows</source>
      <translation>тримати над іншими вікнами</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="54"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="31"/>
      <source>window mode</source>
      <extracomment>A settings title.</extracomment>
      <translation>віконний режим</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="32"/>
      <source>Whether to run PhotoQt in window mode or fullscreen.</source>
      <translation>Визначає, чи запускати PhotoQt у віконному режимі чи в повноекранному режимі.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="40"/>
      <source>run in window mode</source>
      <translation>запустити у віконному режимі</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="45"/>
      <source>show window decoration</source>
      <translation>показати декорацію вікна</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="33"/>
      <source>custom context menu entries</source>
      <extracomment>A settings title.</extracomment>
      <translation>настроювані записи в контекстному меню</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="34"/>
      <source>Add some custom entries to the context menu.</source>
      <translation>Додати деякі записи в контекстне меню.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="57"/>
      <source>Set entries for other image related applications</source>
      <translation>Встановити записи для інших додатків, пов'язаних з зображеннями</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="59"/>
      <source>This will look for some other image related applications on your computer and add an entry for any that are found.</source>
      <translation>Буде виконано пошук деяких інших програм, пов’язаних із зображеннями, на вашому комп’ютері та додано записи для всіх знайдених.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="60"/>
      <source>Note that this will replace all entries currently set and cannot be undone.</source>
      <translation>Зауважте, що це замінить усі поточні налаштовані записи та не може бути скасовано.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="89"/>
      <source>what string to show in main menu</source>
      <extracomment>this is the placeholder text inside of a text box telling the user what text they can enter here</extracomment>
      <translation>який рядок показувати в головному меню</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="113"/>
      <source>which command to execute</source>
      <extracomment>this is the placeholder text inside of a text box telling the user what text they can enter here</extracomment>
      <translation>яку команду виконувати</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="137"/>
      <source>quit</source>
      <extracomment>Keep string short! Used on checkbox for contextmenu, refers to option to close PhotoQt after respective command has been executed.</extracomment>
      <translation>вихід</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="157"/>
      <source>move entry down</source>
      <extracomment>contextmenu settings: used as in &apos;move this entry down in the list of all entries&apos;</extracomment>
      <translation>перемістити запис вниз</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="170"/>
      <source>move entry up</source>
      <extracomment>contextmenu settings: used as in &apos;move this entry up in the list of all entries&apos;</extracomment>
      <translation>перемістити запис вгору</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="188"/>
      <source>delete entry</source>
      <extracomment>contextmenu settings: used as in &apos;delete this entry out of the list of all entries&apos;</extracomment>
      <translation>видалити запис</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="216"/>
      <source>Also show entries in main menu</source>
      <translation>Також показувати записи в головному меню</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="31"/>
      <source>navigation buttons</source>
      <extracomment>A settings title.</extracomment>
      <translation>кнопки навігації</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="32"/>
      <source>Some buttons to help with navigation. These can come in handy when, e.g., operating with a touch screen.</source>
      <translation>Деякі кнопки допоможуть з навігацією. Вони можуть стати в нагоді, коли ви працюєте з сенсорним екраном.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="41"/>
      <source>buttons next to window buttons</source>
      <translation>кнопки поруч із кнопками вікна</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="46"/>
      <source>floating buttons</source>
      <translation>плавучі кнопки</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopoutWhenSmall.qml" line="30"/>
      <source>popout when needed</source>
      <extracomment>A settings title.</extracomment>
      <translation>спливати коли необхідно</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopoutWhenSmall.qml" line="31"/>
      <source>Some elements require a minimum amount of space. When the window is smaller than that, those elements can be automatically popped out to provide the space they need.</source>
      <translation>Деякі елементи потребують мінімальної кількості простору. Коли вікно менше, ніж таке, ці елементи можуть автоматично з'являтися, щоб забезпечити потрібний простір.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopoutWhenSmall.qml" line="36"/>
      <source>pop out when application window is small</source>
      <translation>вискочити, коли вікно програми маленьке</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_metadata</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTags.qml" line="31"/>
      <source>face tags</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>теги обличчя</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTags.qml" line="33"/>
      <source>Whether to show face tags (stored in metadata info).</source>
      <extracomment>The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>Чи показувати теги обличчя (зберігається в метаданих).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTags.qml" line="38"/>
      <location filename="../qml/settingsmanager/tabs/metadata/PQRotation.qml" line="37"/>
      <source>enable</source>
      <translation>увімкнути</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="32"/>
      <source>face tags - border</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>теги обличчя - рамка</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="33"/>
      <source>If and what style of border to show around tagged faces.</source>
      <translation>Якщо та який стиль рамки показувати навколо позначених облич.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="45"/>
      <source>show border</source>
      <extracomment>The border here is the border around face tags.</extracomment>
      <translation>показати рамку</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="96"/>
      <source>click to change color</source>
      <translation>натисніть, щоб змінити колір</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="110"/>
      <source>please choose a color</source>
      <translation>будь ласка оберіть колір</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsFontSize.qml" line="31"/>
      <source>face tags - font size</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>теги обличчя - розмір шрифту</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsFontSize.qml" line="33"/>
      <source>The font size of the name labels.</source>
      <extracomment>The name labels here are the labels with the name used for the face tags.</extracomment>
      <translation>Розмір шрифту назви ярликів.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="31"/>
      <source>face tags - visibility</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>теги обличчя - видимість</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="32"/>
      <source>When to show the face tags and for how long.</source>
      <translation>Коли показувати теги обличчя та як довго.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="38"/>
      <source>hybrid mode</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation>гібридний режим</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="40"/>
      <source>always show all</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation>завжди показувати все</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="42"/>
      <source>show one on hover</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation>показати один при наведенні</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="44"/>
      <source>show all on hover</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation>показати всі при наведенні</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQGPSMap.qml" line="31"/>
      <source>GPS online map</source>
      <extracomment>A settings title.</extracomment>
      <translation>Онлайн карта GPS</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQGPSMap.qml" line="32"/>
      <source>Which map service to use when a GPS position is clicked.</source>
      <translation>Який картографічний сервіс використовувати, якщо натиснути позицію GPS.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="31"/>
      <source>meta information</source>
      <extracomment>A settings title.</extracomment>
      <translation>метаінформація</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="32"/>
      <source>Which meta information to extract and display.</source>
      <translation>Яку метаінформацію видобувати та відображати.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="35"/>
      <source>file name</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>ім'я файлу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="37"/>
      <source>file type</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>тип файлу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="39"/>
      <source>file size</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>розмір файлу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="41"/>
      <source>image #/#</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>зображення #/#</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="43"/>
      <source>dimensions</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>розміри</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="45"/>
      <source>copyright</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>копірайт</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="47"/>
      <source>exposure time</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>час експозиції</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="49"/>
      <source>flash</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>спалах</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="51"/>
      <source>focal length</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>фокусна відстань</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="53"/>
      <source>f-number</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>діафрагма</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="55"/>
      <source>GPS position</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Положення GPS</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="58"/>
      <source>keywords</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>ключові слова</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="60"/>
      <source>light source</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>джерело світла</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="62"/>
      <source>location</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>місцеположення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="64"/>
      <source>make</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>виробник</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="66"/>
      <source>model</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>модель</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="68"/>
      <source>scene type</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>тип сцени</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="70"/>
      <source>software</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>програмне забезпечення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="72"/>
      <source>time photo was taken</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>час зйомки фото</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQRotation.qml" line="30"/>
      <source>auto-rotation</source>
      <extracomment>A settings title.</extracomment>
      <translation>автоповорот</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQRotation.qml" line="31"/>
      <source>Automatically rotate images based on metadata information.</source>
      <translation>Автоматично обертати зображення на основі інформації з метаданих.</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_shortcuts</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="61"/>
      <source>quit</source>
      <extracomment>checkbox in shortcuts settings, used as in: quit PhotoQt. Please keep as short as possible!</extracomment>
      <translation>вихід</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="96"/>
      <source>Click to change shortcut</source>
      <translation>Натисніть, щоб змінити гарячу клавішу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="130"/>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="144"/>
      <source>Click to delete shortcut</source>
      <translation>Натисніть, щоб видалити гарячу клавішу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="89"/>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="61"/>
      <source>no shortcut set</source>
      <translation>не встановлено гарячу клавішу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="71"/>
      <source>Click to manage shortcut</source>
      <translation>Натисніть для керування гарячими клавішами</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="172"/>
      <source>add new</source>
      <extracomment>Used as in &apos;add new shortcut&apos;. Please keep short!</extracomment>
      <translation>додати нове</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQNewShortcut.qml" line="113"/>
      <source>Add New Shortcut</source>
      <translation>Додати нову гарячу клавішу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQNewShortcut.qml" line="134"/>
      <source>Perform a mouse gesture here or press any key combo</source>
      <translation>Виконайте тут жест миші або натисніть будь-яку комбінацію клавіш</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalContainer.qml" line="47"/>
      <source>Add new</source>
      <extracomment>Used on button as in &apos;add new external shortcut&apos;. Please keep short!</extracomment>
      <translation>Додати нове</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_thumbnails</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCache.qml" line="30"/>
      <source>thumbnail cache</source>
      <extracomment>A settings title.</extracomment>
      <translation>кеш мініатюр</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCache.qml" line="31"/>
      <source>Thumbnails can be cached (permanently), following the freedesktop.org standard.</source>
      <translation>Мініатюри можна кешувати (назавжди) відповідно до стандарту freedesktop.org.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCache.qml" line="36"/>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="45"/>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="46"/>
      <source>enable</source>
      <translation>увімкнути</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCenter.qml" line="30"/>
      <source>keep in center</source>
      <extracomment>A settings title. Used as in: Keep thumbnail for current main image in center.</extracomment>
      <translation>тримати в центрі</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCenter.qml" line="31"/>
      <source>Keep currently active thumbnail in the center of the screen</source>
      <translation>Зберігати поточну активну мініатюру в центрі екрана</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCenter.qml" line="35"/>
      <source>center on active thumbnail</source>
      <translation>у центрі активної мініатюри</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQDisable.qml" line="29"/>
      <source>disable thumbnails</source>
      <translation>вимкнути мініатюри</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQDisable.qml" line="30"/>
      <source>Disable thumbnails in case no thumbnails are desired whatsoever.</source>
      <translation>Вимкнути мініатюри, якщо мініатюри взагалі не потрібні.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQDisable.qml" line="35"/>
      <source>disable all thumbnails</source>
      <translation>вимкнути всі мініатюри</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="30"/>
      <source>filename label</source>
      <extracomment>A settings title. The filename label here is the one that is written on thumbnails.</extracomment>
      <translation>мітка імені файлу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="31"/>
      <source>Show the filename on a small label on the thumbnail image.</source>
      <translation>Показувати назву файлу на невеликій мітці на мініатюрі.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="57"/>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="58"/>
      <source>font size:</source>
      <translation>розмір шрифту:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="30"/>
      <source>filename-only</source>
      <extracomment>A settings title. This refers to using only the filename as thumbnail and no actual image.</extracomment>
      <translation>лише ім'я файлу</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="31"/>
      <source>Show only the filename as thumbnail, no actual image.</source>
      <translation>Показувати лише назву файлу як мініатюру, без фактичного зображення.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQLiftUp.qml" line="30"/>
      <source>lift up</source>
      <extracomment>A settings title. This refers to the lift up of thumbnail images when active/hovered.</extracomment>
      <translation>підняти вгору</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQLiftUp.qml" line="31"/>
      <source>How many pixels to lift up thumbnails when either hovered or active.</source>
      <translation>На скільки пікселів піднімати мініатюри, коли вони наведені або активні.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="30"/>
      <source>position</source>
      <extracomment>A settings title referring to the position of the thumbnails (upper or lower edge of PhotoQt).</extracomment>
      <translation>положення</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="31"/>
      <source>Which edge to show the thumbnails on, upper or lower edge.</source>
      <translation>На якому краю відображати мініатюри, верхньому чи нижньому.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="38"/>
      <source>upper edge</source>
      <extracomment>The upper edge of PhotoQt</extracomment>
      <translation>верхній край</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="40"/>
      <source>lower edge</source>
      <extracomment>The lower edge of PhotoQt</extracomment>
      <translation>нижній край</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSize.qml" line="30"/>
      <source>size</source>
      <extracomment>A settings title referring to the size of the thumbnails.</extracomment>
      <translation>розмір</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSize.qml" line="31"/>
      <source>How large (or small) the thumbnails should be.</source>
      <translation>Наскільки великими (чи маленькими) мають бути мініатюри.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSpacing.qml" line="30"/>
      <source>spacing</source>
      <extracomment>A settings title referring to the spacing of thumbnails, i.e., how much empty space to have between each.</extracomment>
      <translation>інтервал</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSpacing.qml" line="31"/>
      <source>How much space to show between the thumbnails.</source>
      <translation>Скільки місця для показу між мініатюрами.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQThreads.qml" line="30"/>
      <source>threads</source>
      <extracomment>A settings title, as in: How many threads to use to generate thumbnails.</extracomment>
      <translation>потоки</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQThreads.qml" line="31"/>
      <source>How many threads to use to create thumbnails. Too many threads can slow down your computer!</source>
      <translation>Скільки потоків використовувати для створення мініатюр. Забагато потоків може уповільнити ваш комп'ютер!</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQThreads.qml" line="50"/>
      <source>Threads:</source>
      <translation>Потоки:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="30"/>
      <source>visibility</source>
      <extracomment>A settings title referring to the visibility of the thumbnails, i.e., if and when to hide them.</extracomment>
      <translation>видимість</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="31"/>
      <source>If and how to keep thumbnails visible</source>
      <translation>Якщо і як показувати мініатюри</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="37"/>
      <source>hide when not needed</source>
      <extracomment>This is talking about the thumbnails.</extracomment>
      <translation>приховати, коли не потрібно</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="39"/>
      <source>never hide</source>
      <extracomment>This is talking about the thumbnails.</extracomment>
      <translation>ніколи не приховувати</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="41"/>
      <source>hide when zoomed in</source>
      <extracomment>This is talking about the thumbnails.</extracomment>
      <translation>сховати при збільшенні</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="29"/>
      <source>exclude folders</source>
      <translation>виключити папки</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="30"/>
      <source>Exclude the specified folders and all of its subfolders from any sort of caching and preloading.</source>
      <translation>Виключати зазначені папки та всі їх вкладені папки з будь-якого виду кешування та попереднього завантаження.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="103"/>
      <source>Do not cache these folders:</source>
      <translation>Не кешувати ці папки:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="115"/>
      <source>Add folder</source>
      <extracomment>Written on a button</extracomment>
      <translation>Додати папку</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="110"/>
      <source>One folder per line</source>
      <translation>Одна папка на рядок</translation>
    </message>
  </context>
  <context>
    <name>slideshow</name>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="114"/>
      <source>Click to go to the previous image</source>
      <translation>Натисніть, щоб перейти до попереднього зображення</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="144"/>
      <source>Click to pause slideshow</source>
      <translation>Натисніть, щоб призупинити показ слайдів</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="145"/>
      <source>Click to play slideshow</source>
      <translation>Натисніть, щоб відтворити слайд-шоу</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="171"/>
      <source>Click to go to the next image</source>
      <translation>Натисніть, щоб перейти до наступного зображення</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="200"/>
      <source>Click to exit slideshow</source>
      <translation>Натисніть для виходу зі слайд-шоу</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="248"/>
      <source>Sound volume:</source>
      <translation>Гучність звуку:</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="61"/>
      <location filename="../qml/slideshow/PQSlideShowSettingsPopout.qml" line="34"/>
      <source>Slideshow settings</source>
      <extracomment>Window title</extracomment>
      <translation>Налаштування слайд-шоу</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="110"/>
      <source>interval</source>
      <extracomment>The interval between images in a slideshow</extracomment>
      <translation>інтервал</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="147"/>
      <source>animation</source>
      <extracomment>This is referring to the in/out animation of images during a slideshow</extracomment>
      <translation>анімація</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="159"/>
      <source>opacity</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>непрозорість</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="161"/>
      <source>along x-axis</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>вздовж осі х</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="163"/>
      <source>along y-axis</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>вздовж осі у</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="165"/>
      <source>rotation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>обертання</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="167"/>
      <source>explosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>вибух</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="169"/>
      <source>implosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>імплозія</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="171"/>
      <source>choose one at random</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>виберіть один навмання</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="191"/>
      <source>animation speed</source>
      <extracomment>The speed of transitioning from one image to another during slideshows</extracomment>
      <translation>швидкість анімації</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="211"/>
      <source>immediately, without animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>негайно, без анімації</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="214"/>
      <source>pretty fast animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>досить швидка анімація</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="217"/>
      <source>not too fast and not too slow</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>не надто швидко і не надто повільно</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="219"/>
      <source>very slow animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>дуже повільна анімація</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="226"/>
      <source>current speed</source>
      <extracomment>This refers to the currently set speed of transitioning from one image to another during slideshows</extracomment>
      <translation>поточна швидкість</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="248"/>
      <source>looping</source>
      <translation>зациклювання</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="261"/>
      <source>loop over all files</source>
      <extracomment>Loop over all images during slideshows</extracomment>
      <translation>цикл серед усіх файлів</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="278"/>
      <source>shuffle</source>
      <extracomment>during slideshows shuffle the order of all images</extracomment>
      <translation>перемішати</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="291"/>
      <source>shuffle all files</source>
      <extracomment>during slideshows shuffle the order of all images</extracomment>
      <translation>перемішати всі файли</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="308"/>
      <source>subfolders</source>
      <extracomment>also include images in subfolders during slideshows</extracomment>
      <translation>підпапки</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="321"/>
      <source>include images in subfolders</source>
      <extracomment>also include images in subfolders during slideshows</extracomment>
      <translation>включати зображення у підпапках</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="339"/>
      <source>file info</source>
      <extracomment>What to do with the file details during slideshows</extracomment>
      <translation>інформація про файл</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="352"/>
      <source>hide label with details about current file</source>
      <extracomment>What to do with the file details during slideshows</extracomment>
      <translation>приховати ярлик з деталями про поточний файл</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="370"/>
      <source>window buttons</source>
      <extracomment>What to do with the window buttons during slideshows</extracomment>
      <translation>віконні кнопки</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="383"/>
      <source>hide window buttons during slideshow</source>
      <extracomment>What to do with the window buttons during slideshows</extracomment>
      <translation>сховати кнопки вікон під час слайд-шоу</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="401"/>
      <source>music</source>
      <extracomment>The music that is to be played during slideshows</extracomment>
      <translation>музика</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="418"/>
      <source>enable music</source>
      <extracomment>Enable music to be played during slideshows</extracomment>
      <translation>увімкнути музику</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="425"/>
      <source>no file selected</source>
      <translation>не вибрано жодного файлу</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="427"/>
      <source>Click to select music file</source>
      <translation>Натисніть для вибору музичного файлу</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="428"/>
      <source>Click to change music file</source>
      <translation>Натисніть, щоб змінити музичний файл</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="439"/>
      <source>Common music file formats</source>
      <translation>Загальні музичні формати</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="440"/>
      <source>All Files</source>
      <translation>Всі файли</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="475"/>
      <source>Start slideshow</source>
      <extracomment>Written on a clickable button</extracomment>
      <translation>Почати слайд-шоу</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControlsPopout.qml" line="34"/>
      <source>Slideshow controls</source>
      <extracomment>Window title</extracomment>
      <translation>Елементи управління слайд-шоу</translation>
    </message>
  </context>
  <context>
    <name>startup</name>
    <message>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="323"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="325"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="327"/>
      <source>Edit with %1</source>
      <extracomment>Used as in &apos;Edit with [application]&apos;. %1 will be replaced with application name.</extracomment>
      <translation>Редагувати за допомогою %1</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="329"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="331"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="333"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="335"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="337"/>
      <source>Open in %1</source>
      <extracomment>Used as in &apos;Open with [application]&apos;. %1 will be replaced with application name.</extracomment>
      <translation>Відкрити в %1</translation>
    </message>
  </context>
  <context>
    <name>streaming</name>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="98"/>
      <source>Scan for devices</source>
      <extracomment>Used as tooltip for button that starts a scan for Chromecast streaming devices in the local network</extracomment>
      <translation>Пошук пристроїв</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="205"/>
      <source>Disconnect</source>
      <extracomment>Written on button, as in &apos;Disconnect from connected Chromecast streaming device&apos;</extracomment>
      <translation>Відключити</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="207"/>
      <source>Connect</source>
      <extracomment>Written on button, as in &apos;Connect to Chromecast streaming device&apos;</extracomment>
      <translation>Підключити</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="240"/>
      <source>searching for devices...</source>
      <extracomment>status text while searching for chromecast streaming devices in the local network</extracomment>
      <translation>пошук пристроїв...</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="242"/>
      <source>no devices found</source>
      <extracomment>result of scan for chromecast streaming devices</extracomment>
      <translation>пристроїв не знайдено</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecastPopout.qml" line="34"/>
      <source>Streaming (Chromecast)</source>
      <extracomment>Window title</extracomment>
      <translation>Потокова передача (Chromecast)</translation>
    </message>
  </context>
  <context>
    <name>thumbnailbar</name>
    <message>
      <location filename="../qml/mainwindow/PQThumbnailBar.qml" line="249"/>
      <source>File size:</source>
      <translation>Розмір файлу:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQThumbnailBar.qml" line="250"/>
      <source>File type:</source>
      <translation>Тип файлу:</translation>
    </message>
  </context>
  <context>
    <name>unavailable</name>
    <message>
      <location filename="../qml/unavailable/PQUnavailable.qml" line="74"/>
      <location filename="../qml/unavailable/PQUnavailablePopout.qml" line="75"/>
      <source>Sorry, but this feature is not yet available on Windows.</source>
      <translation>Вибачте, але ця функція ще не доступна в Windows.</translation>
    </message>
    <message>
      <location filename="../qml/unavailable/PQUnavailablePopout.qml" line="34"/>
      <source>Feature unavailable</source>
      <extracomment>Window title, informing user that the requested feature is currently not available</extracomment>
      <translation>Функція недоступна</translation>
    </message>
  </context>
  <context>
    <name>wallpaper</name>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="71"/>
      <source>Warning: %1 module not activated</source>
      <translation>Попередження: %1 модуль не активовано</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="80"/>
      <location filename="../qml/wallpaper/ele/PQGnome.qml" line="69"/>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="72"/>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="81"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="69"/>
      <source>Warning: %1 not found</source>
      <translation>Попередження: %1 не знайдено</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="102"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="91"/>
      <source>Set to which screens</source>
      <extracomment>As in: Set wallpaper to which screens</extracomment>
      <translation>Встановити для яких екранів</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="115"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="103"/>
      <source>Screen</source>
      <extracomment>Used in wallpaper element</extracomment>
      <translation>Екран</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="140"/>
      <source>Set to which workspaces</source>
      <extracomment>Enlightenment desktop environment handles wallpapers per workspace (different from screen)</extracomment>
      <translation>Встановити в які робочі області</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="154"/>
      <source>Workspace:</source>
      <extracomment>Enlightenment desktop environment handles wallpapers per workspace (different from screen)</extracomment>
      <translation>Робоча область:</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQGnome.qml" line="83"/>
      <location filename="../qml/wallpaper/ele/PQWindows.qml" line="66"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="128"/>
      <source>Choose picture option</source>
      <extracomment>picture option refers to how to format a pictrue when setting it as wallpaper</extracomment>
      <translation>Вибір режиму зображення</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="57"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="180"/>
      <source>Other</source>
      <extracomment>Used as in: Other Desktop Environment</extracomment>
      <translation>Інше</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="100"/>
      <source>Tool:</source>
      <extracomment>Tool refers to a program that can be executed</extracomment>
      <translation>Інструмент:</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="106"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="124"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="142"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="160"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="178"/>
      <source>Click to choose %1</source>
      <extracomment>%1 is a placeholder for the name of a desktop environment (plasma, xfce, gnome, etc.)</extracomment>
      <translation>Натисніть, щоб вибрати %1</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="207"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="226"/>
      <location filename="../qml/wallpaper/PQWallpaperPopout.qml" line="34"/>
      <source>Set as Wallpaper</source>
      <extracomment>Heading of wallpaper element
----------
Written on clickable button
----------
Window title</extracomment>
      <translation>Встановити як шпалери</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQPlasma.qml" line="67"/>
      <source>The image will be set to all screens at the same time.</source>
      <translation>Зображення буде встановлено на всіх екранах одночасно.</translation>
    </message>
  </context>
</TS>
