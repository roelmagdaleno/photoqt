/**************************************************************************
 **                                                                      **
 ** Copyright (C) 2011-2022 Lukas Spies                                  **
 ** Contact: https://photoqt.org                                         **
 **                                                                      **
 ** This file is part of PhotoQt.                                        **
 **                                                                      **
 ** PhotoQt is free software: you can redistribute it and/or modify      **
 ** it under the terms of the GNU General Public License as published by **
 ** the Free Software Foundation, either version 2 of the License, or    **
 ** (at your option) any later version.                                  **
 **                                                                      **
 ** PhotoQt is distributed in the hope that it will be useful,           **
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of       **
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        **
 ** GNU General Public License for more details.                         **
 **                                                                      **
 ** You should have received a copy of the GNU General Public License    **
 ** along with PhotoQt. If not, see <http://www.gnu.org/licenses/>.      **
 **                                                                      **
 **************************************************************************/

import QtQuick 2.9
import Qt.labs.platform 1.0

import "../elements"
import "../shortcuts/handleshortcuts.js" as HandleShortcuts

Item {

    id: slideshowsettings_top

    width: parentWidth
    height: parentHeight

    property int parentWidth: toplevel.width
    property int parentHeight: toplevel.height

    opacity: PQSettings.interfacePopoutSlideShowSettings ? 1 : 0
    Behavior on opacity { NumberAnimation { duration: PQSettings.interfacePopoutSlideShowSettings ? 0 : PQSettings.imageviewAnimationDuration*100 } }
    visible: opacity!=0
    enabled: visible

    Rectangle {

        anchors.fill: parent
        color: "#f41f1f1f"

        PQMouseArea {
            anchors.fill: parent
            hoverEnabled: true
            enabled: !PQSettings.interfacePopoutSlideShowSettings
            onClicked:
                button_cancel.clicked()
        }

        Text {
            id: heading
            y: insidecont.y-height
            width: parent.width
            text: em.pty+qsTranslate("slideshow", "Slideshow settings")
            font.pointSize: 25
            font.bold: true
            color: "white"
            horizontalAlignment: Text.AlignHCenter
        }

        PQMouseArea {
            anchors.fill: insidecont
            anchors.margins: -50
            hoverEnabled: true
        }

        Item {

            id: insidecont

            x: ((parent.width-width)/2)
            y: ((parent.height-height)/2)
            width: childrenRect.width
            height: childrenRect.height

            clip: true

            Column {

                id: col

                property int leftcolwidth: 100

                spacing: 15

                Item {
                    width: 1
                    height: 20
                }

                Row {

                    spacing: 15

                    height: childrenRect.height

                    Text {
                        id: interval_txt
                        color: "white"
                        font.pointSize: 15
                        font.bold: true
                        //: The interval between images in a slideshow
                        text: em.pty+qsTranslate("slideshow", "interval") + ":"
                        horizontalAlignment: Text.AlignRight
                        Component.onCompleted: {
                            if(width > col.leftcolwidth)
                                col.leftcolwidth = width
                            width = Qt.binding(function() { return col.leftcolwidth; })
                        }
                    }

                    PQSlider {
                        id: interval_slider
                        y: (interval_txt.height-height)/2
                        from: 1
                        to: 300
                        toolTipSuffix: "s"
                    }

                    Text {
                        y: (interval_txt.height-height)/2
                        color: "white"
                        text: interval_slider.value+"s"
                    }

                }

                Row {

                    spacing: 15

                    height: childrenRect.height

                    Text {
                        id: animtype_txt
                        color: "white"
                        font.pointSize: 15
                        font.bold: true
                        //: This is referring to the in/out animation of images during a slideshow
                        text: em.pty+qsTranslate("slideshow", "animation") + ":"
                        horizontalAlignment: Text.AlignRight
                        Component.onCompleted: {
                            if(width > col.leftcolwidth)
                                col.leftcolwidth = width
                            width = Qt.binding(function() { return col.leftcolwidth; })
                        }
                    }

                    PQComboBox {
                        id: animtype_combo
                        //: This is referring to the in/out animation of images during slideshows
                        model: [em.pty+qsTranslate("slideshow", "opacity"),
                                //: This is referring to the in/out animation of images during slideshows
                                em.pty+qsTranslate("slideshow", "along x-axis"),
                                //: This is referring to the in/out animation of images during slideshows
                                em.pty+qsTranslate("slideshow", "along y-axis"),
                                //: This is referring to the in/out animation of images
                                em.pty+qsTranslate("slideshow", "rotation"),
                                //: This is referring to the in/out animation of images
                                em.pty+qsTranslate("slideshow", "explosion"),
                                //: This is referring to the in/out animation of images
                                em.pty+qsTranslate("slideshow", "implosion"),
                                //: This is referring to the in/out animation of images
                                em.pty+qsTranslate("slideshow", "choose one at random")]
                        lineBelowItem: 5
                    }

                }


                Row {

                    spacing: 15

                    height: childrenRect.height

                    Text {
                        id: trans_txt
                        verticalAlignment: Text.AlignTop
                        color: "white"
                        font.pointSize: 15
                        font.bold: true
                        //: The speed of transitioning from one image to another during slideshows
                        text: em.pty+qsTranslate("slideshow", "animation speed") + ":"
                        horizontalAlignment: Text.AlignRight
                        Component.onCompleted: {
                            if(width > col.leftcolwidth)
                                col.leftcolwidth = width
                            width = Qt.binding(function() { return col.leftcolwidth; })
                        }
                    }

                    Column {

                        spacing: 10

                        PQSlider {
                            id: transition_slider
                            height: trans_txt.height
                            from: 0
                            to: 15
                            tooltip: (value == 15 ?
                                         //: This refers to a speed of transitioning from one image to another during slideshows
                                         em.pty+qsTranslate("slideshow", "immediately, without animation") :
                                         (value > 9 ?
                                              //: This refers to a speed of transitioning from one image to another during slideshows
                                              em.pty+qsTranslate("slideshow", "pretty fast animation") :
                                              (value > 4 ?
                                                   //: This refers to a speed of transitioning from one image to another during slideshows
                                                   em.pty+qsTranslate("slideshow", "not too fast and not too slow") :
                                                   //: This refers to a speed of transitioning from one image to another during slideshows
                                                   em.pty+qsTranslate("slideshow", "very slow animation"))))
                        }

                        Text {
                            id: transspeed_txt
                            color: "white"
                            //: This refers to the currently set speed of transitioning from one image to another during slideshows
                            text: em.pty+qsTranslate("slideshow", "current speed") + ": <b>" + transition_slider.tooltip + "</b>"
                        }

                    }

                }





                Row {

                    spacing: 15

                    height: childrenRect.height

                    Text {
                        id: loop_txt
                        color: "white"
                        font.pointSize: 15
                        font.bold: true
                        text: em.pty+qsTranslate("slideshow", "looping") + ":"
                        horizontalAlignment: Text.AlignRight
                        Component.onCompleted: {
                            if(width > col.leftcolwidth)
                                col.leftcolwidth = width
                            width = Qt.binding(function() { return col.leftcolwidth; })
                        }
                    }

                    PQCheckbox {
                        id: loop_check
                        y: (loop_txt.height-height)/2
                        //: Loop over all images during slideshows
                        text: em.pty+qsTranslate("slideshow", "loop over all files")
                    }

                }

                Row {

                    spacing: 15

                    height: childrenRect.height

                    Text {
                        id: shuffle_txt
                        color: "white"
                        font.pointSize: 15
                        font.bold: true
                        //: during slideshows shuffle the order of all images
                        text: em.pty+qsTranslate("slideshow", "shuffle") + ":"
                        horizontalAlignment: Text.AlignRight
                        Component.onCompleted: {
                            if(width > col.leftcolwidth)
                                col.leftcolwidth = width
                            width = Qt.binding(function() { return col.leftcolwidth; })
                        }
                    }

                    PQCheckbox {
                        id: shuffle_check
                        y: (shuffle_txt.height-height)/2
                        //: during slideshows shuffle the order of all images
                        text: em.pty+qsTranslate("slideshow", "shuffle all files")
                    }

                }

                Row {

                    spacing: 15

                    height: childrenRect.height

                    Text {
                        id: subfolders_txt
                        color: "white"
                        font.pointSize: 15
                        font.bold: true
                        //: also include images in subfolders during slideshows
                        text: em.pty+qsTranslate("slideshow", "subfolders") + ":"
                        horizontalAlignment: Text.AlignRight
                        Component.onCompleted: {
                            if(width > col.leftcolwidth)
                                col.leftcolwidth = width
                            width = Qt.binding(function() { return col.leftcolwidth; })
                        }
                    }

                    PQCheckbox {
                        id: subfolders_check
                        y: (shuffle_txt.height-height)/2
                        //: also include images in subfolders during slideshows
                        text: em.pty+qsTranslate("slideshow", "include images in subfolders")
                    }

                }

                Row {

                    spacing: 15

                    height: childrenRect.height

                    Text {
                        id: quick_txt
                        verticalAlignment: Text.AlignTop
                        color: "white"
                        font.pointSize: 15
                        font.bold: true
                        //: What to do with the file details during slideshows
                        text: em.pty+qsTranslate("slideshow", "file info") + ":"
                        horizontalAlignment: Text.AlignRight
                        Component.onCompleted: {
                            if(width > col.leftcolwidth)
                                col.leftcolwidth = width
                            width = Qt.binding(function() { return col.leftcolwidth; })
                        }
                    }

                    PQCheckbox {
                        id: quick_check
                        y: (quick_txt.height-height)/2
                        //: What to do with the file details during slideshows
                        text: em.pty+qsTranslate("slideshow", "hide label with details about current file")
                    }

                }

                Row {

                    spacing: 15

                    height: childrenRect.height

                    Text {
                        id: winbut_txt
                        verticalAlignment: Text.AlignTop
                        color: "white"
                        font.pointSize: 15
                        font.bold: true
                        //: What to do with the window buttons during slideshows
                        text: em.pty+qsTranslate("slideshow", "window buttons") + ":"
                        horizontalAlignment: Text.AlignRight
                        Component.onCompleted: {
                            if(width > col.leftcolwidth)
                                col.leftcolwidth = width
                            width = Qt.binding(function() { return col.leftcolwidth; })
                        }
                    }

                    PQCheckbox {
                        id: winbut_check
                        y: (winbut_txt.height-height)/2
                        //: What to do with the window buttons during slideshows
                        text: em.pty+qsTranslate("slideshow", "hide window buttons during slideshow")
                    }

                }

                Row {

                    spacing: 15

                    height: childrenRect.height

                    Text {
                        id: music_txt
                        verticalAlignment: Text.AlignTop
                        color: "white"
                        font.pointSize: 15
                        font.bold: true
                        //: The music that is to be played during slideshows
                        text: em.pty+qsTranslate("slideshow", "music") + ":"
                        horizontalAlignment: Text.AlignRight
                        Component.onCompleted: {
                            if(width > col.leftcolwidth)
                                col.leftcolwidth = width
                            width = Qt.binding(function() { return col.leftcolwidth; })
                        }
                    }

                    Column {

                        spacing: 10

                        PQCheckbox {
                            id: music_check
                            height: music_txt.height
                            //: Enable music to be played during slideshows
                            text: em.pty+qsTranslate("slideshow", "enable music")
                        }

                        PQButton {
                            id: music_button
                            enabled: music_check.checked
                            property string musicfile: ""
                            text: musicfile=="" ? "[" + em.pty+qsTranslate("slideshow", "no file selected") + "]" : handlingFileDir.getFileNameFromFullPath(musicfile)
                            tooltip: (musicfile==""
                                        ? em.pty+qsTranslate("slideshow", "Click to select music file")
                                        : ("<b>"+musicfile+"</b><br><br>" + em.pty+qsTranslate("slideshow", "Click to change music file")))
                            onClicked: {
                                fileDialog.visible = true
                            }
                        }

                        FileDialog {
                            id: fileDialog
                            currentFile: music_button.musicfile=="" ? "" : music_button.musicfile
                            folder: (music_button.musicfile == "" ? "file://"+handlingFileDir.getHomeDir() : "file://"+handlingFileDir.getFilePathFromFullPath(music_button.musicfile))
                            modality: Qt.ApplicationModal
                            nameFilters: [em.pty+qsTranslate("slideshow", "Common music file formats") + " (aac *.flac *.mp3 *.ogg *.oga *.wav *.wma)",
                                          em.pty+qsTranslate("slideshow", "All Files") + " (*.*)"]
                            onAccepted: {
                                if(fileDialog.file != "")
                                    music_button.musicfile = handlingFileDir.cleanPath(fileDialog.file)
                            }
                        }

                    }

                }

                Item {
                    width: 1
                    height: 5
                }

            }

        }

        Row {

            id: button_row

            spacing: 5

            y: insidecont.y+insidecont.height
            x: (parent.width-width)/2

            height: button_start.height+20

            PQButton {
                id: button_start
                y: 5
                //: Written on a clickable button
                text: em.pty+qsTranslate("slideshow", "Start slideshow")
                onClicked: {

                    var animArray = ["opacity", "x", "y", "rotation", "explosion", "implosion", "random"]
                    PQSettings.slideshowTypeAnimation = animArray[animtype_combo.currentIndex]

                    PQSettings.slideshowTime = interval_slider.value
                    PQSettings.slideshowImageTransition = transition_slider.value
                    PQSettings.slideshowLoop = loop_check.checked
                    PQSettings.slideshowShuffle = shuffle_check.checked
                    PQSettings.slideshowHideWindowButtons = winbut_check.checked
                    PQSettings.slideshowHideLabels = quick_check.checked
                    PQSettings.slideshowMusicFile = (music_check.checked&&music_button.musicfile!="" ? music_button.musicfile : "")
                    PQSettings.slideshowIncludeSubFolders = subfolders_check.checked

                    if(PQSettings.interfacePopoutSlideShowSettings) {
                        slideshow_window.visible = false
                    } else {
                        slideshowsettings_top.opacity = 0
                        variables.visibleItem = ""
                    }
                    loader.ensureItIsReady("slideshowcontrols")
                    loader.passOn("slideshowcontrols", "start", undefined)
                }
            }
            PQButton {
                id: button_cancel
                y: 5
                text: genericStringCancel
                onClicked: {
                    if(PQSettings.interfacePopoutSlideShowSettings) {
                        slideshow_window.visible = false
                    } else {
                        slideshowsettings_top.opacity = 0
                        variables.visibleItem = ""
                    }
                }
            }

        }

        Image {
            x: 5
            y: 5
            width: 15
            height: 15
            source: "/popin.svg"
            sourceSize: Qt.size(width, height)
            opacity: popinmouse.containsMouse ? 1 : 0.4
            Behavior on opacity { NumberAnimation { duration: 200 } }
            PQMouseArea {
                id: popinmouse
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
                tooltip: PQSettings.interfacePopoutSlideShowSettings ?
                             //: Tooltip of small button to merge a popped out element (i.e., one in its own window) into the main interface
                             em.pty+qsTranslate("popinpopout", "Merge into main interface") :
                             //: Tooltip of small button to show an element in its own window (i.e., not merged into main interface)
                             em.pty+qsTranslate("popinpopout", "Move to its own window")
                onClicked: {
                    if(PQSettings.interfacePopoutSlideShowSettings)
                        slideshow_window.storeGeometry()
                    button_cancel.clicked()
                    PQSettings.interfacePopoutSlideShowSettings = !PQSettings.interfacePopoutSlideShowSettings
                    HandleShortcuts.executeInternalFunction("__slideshow")
                }
            }
        }

        Connections {
            target: loader
            onSlideshowPassOn: {
                if(what == "show") {
                    if(PQSettings.interfacePopoutSlideShowSettings) {
                        slideshow_window.visible = true
                    } else {
                        if(filefoldermodel.current == -1)
                            return
                        opacity = 1
                        variables.visibleItem = "slideshowsettings"
                    }

                    var animArray = ["opacity", "x", "y", "rotation", "explosion", "implosion", "random"]
                    PQSettings.slideshowTypeAnimation = animArray[animtype_combo.currentIndex]
                    animtype_combo.currentIndex = animArray.indexOf(PQSettings.slideshowTypeAnimation)
                    if(animtype_combo.currentIndex == -1) animtype_combo.currentIndex = 0

                    interval_slider.value = PQSettings.slideshowTime
                    transition_slider.value = PQSettings.slideshowImageTransition
                    loop_check.checked = PQSettings.slideshowLoop
                    shuffle_check.checked = PQSettings.slideshowShuffle
                    winbut_check.checked = PQSettings.slideshowHideWindowButtons
                    quick_check.checked = PQSettings.slideshowHideLabels
                    music_check.checked = (PQSettings.slideshowMusicFile!="")
                    music_button.musicfile = PQSettings.slideshowMusicFile
                    subfolders_check.checked = PQSettings.slideshowIncludeSubFolders

                } else if(what == "hide") {
                    button_cancel.clicked()
                } else if(what == "keyevent") {
                    if(param[0] == Qt.Key_Escape)
                        button_cancel.clicked()
                    else if(param[0] == Qt.Key_Enter || param[0] == Qt.Key_Return)
                        button_start.clicked()
                }
            }
        }

    }

}
